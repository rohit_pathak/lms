from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns

from announcement.api import views

urlpatterns = [
    url(r'^$', views.AnnouncementList.as_view(), name='announcement-list'),
    url(r'^(?P<pk>\d+)/$', views.AnnouncementDetail.as_view(), name='announcement-detail'),
]

urlpatterns = format_suffix_patterns(urlpatterns)
