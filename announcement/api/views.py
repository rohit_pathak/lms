import textwrap

from django.core.mail import EmailMessage
from django.core.urlresolvers import reverse
from rest_framework import generics
from rest_framework.permissions import IsAuthenticated

import constants
from announcement.api.serializers import AnnouncementDetailSerializer
from announcement.models import Announcement
from course.api.permissions import course_instructor_or_forbidden, course_member_or_forbidden


class AnnouncementList(generics.CreateAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = AnnouncementDetailSerializer

    def perform_create(self, serializer):
        course_instructor_or_forbidden(self.request.user, serializer.validated_data.get('course'))
        announcement = serializer.save()
        email_subject = '%s: New announcement' % (announcement.course.name,)
        email_body = """
        %s
        %s

        Hello,

        Your instructor posted a new announcement: "%s"
        Read full details at %s.

        Thanks,
        The Coursetrack Team
        """ % (announcement.course.name, announcement.course.academy.name, announcement.title,
               self.request.build_absolute_uri(
                   reverse('course-announcement-detail-page', args=(announcement.course.pk, announcement.pk))))
        email = EmailMessage(subject=email_subject, body=textwrap.dedent(email_body),
                             to=[constants.UNDISCLOSED_RECIPIENTS],
                             bcc=[s.user.email for s in announcement.course.students.all()])
        email.send(fail_silently=True)


class AnnouncementDetail(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = AnnouncementDetailSerializer
    queryset = Announcement.objects.all()

    def perform_update(self, serializer):
        course_instructor_or_forbidden(self.request.user, serializer.validated_data.get('course'))
        serializer.save()

    def perform_destroy(self, instance):
        course_instructor_or_forbidden(self.request.user, instance.course)
        instance.delete()

    def retrieve(self, request, *args, **kwargs):
        announcement = self.get_object()
        course_member_or_forbidden(self.request.user, announcement.course)
        return super(AnnouncementDetail, self).retrieve(request, *args, **kwargs)
