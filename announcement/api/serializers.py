from rest_framework import serializers

from announcement.models import Announcement


class AnnouncementRef(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Announcement
        fields = ('url', 'course', 'title', 'create_date')


class AnnouncementDetailSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Announcement
        fields = ('url', 'course', 'title', 'description', 'create_date')
