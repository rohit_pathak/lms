from __future__ import unicode_literals

from django.db import models

from course.models import Course


class Announcement(models.Model):
    course = models.ForeignKey(Course)
    title = models.CharField(max_length=150)
    description = models.TextField(null=True, blank=True)
    create_date = models.DateTimeField(auto_now_add=True, blank=True)
