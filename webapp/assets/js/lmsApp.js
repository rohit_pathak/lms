'use strict';

_.string = s; // reassign underscore string object 's' to _.string

var app = angular.module('lms', ['ngCookies', 'ngSanitize',
    'ngQuill', 'angularFileUpload', 'ui.sortable'])
    .config(['$httpProvider', function ($httpProvider) {
        $httpProvider.defaults.xsrfCookieName = 'csrftoken';
        $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
    }])
    .config(['ngQuillConfigProvider', function (ngQuillConfigProvider) {
        ngQuillConfigProvider.set(
            {
                modules: {
                    toolbar: [
                        [{'size': ['small', false, 'large', 'huge']}],  // custom dropdown
                        ['bold', 'italic', 'underline'],        // toggled buttons
                        [{'list': 'ordered'}, {'list': 'bullet'}],
                        [{'font': []}],
                        [{'align': []}],
                        ['image'],
                        ['clean']
                    ]
                }
            },
            'snow', // theme
            '...');
    }])
    .filter('trusted', ['$sce', function ($sce) {
        return function (url) {
            return $sce.trustAsResourceUrl(url);
        };
    }]);

// hack to override the rangy focus issue
window.addEventListener("load", function () {
    var elem = document.querySelector(":focus");
    if (elem) {
        elem.blur();
        elem.focus();
    }
});

$(document).ready(function () {
    // stop video when video modal is closed
    $(".modal").on('hidden.bs.modal', function (e) {
        $(".modal iframe").attr("src", $(".modal iframe").attr("src"));
    });
});
