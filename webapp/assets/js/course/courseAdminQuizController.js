'use strict';

(function () {
    angular.module('lms')
        .controller('CourseAdminQuizController', ['courseFactory', CourseAdminQuizController]);

    function CourseAdminQuizController(courseFactory) {
        var vm = this;
        vm.course = null;
        vm.quiz = {};
        vm.newQuestion = {};
        vm.selectedQuestion = {};
        vm.newAnswerChoice = {};
        vm.questions = [];
        vm.selectedQuestion = null;
        vm.questionToDelete = null;
        vm.state = {
            loadingQuiz: false,
            addQuestionIsLoading: false,
            editQuestionIsLoading: false,
            showSubmissions: false,
            showQuiz: true
        };

        vm.questionSortableOptions = {
            handle: '.quiz-question-sort-handle',
            stop: function (e, ui) {
                // update sort_orders in displayed topics
                var i = 0;
                _.each(vm.quiz.quizquestion_set, function (q) {
                    q.sort_order = i;
                    i += 1;
                });
                // now update sort orders on the backend
                courseFactory.bulkUpdateQuizQuestions(vm.quiz.url, vm.quiz.quizquestion_set); // ignore request result
            }
        };

        vm.editQuiz = function () {
            vm.state.loadingQuiz = true;
            courseFactory.updateQuiz(vm.quiz).then(function (quiz) {
                vm.quiz = quiz;
                $('#edit-quiz-modal').modal('hide');
                vm.state.loadingQuiz = false;
            });
        };

        vm.addAnswerChoice = function (question) {
            vm.state.loadingAddAnswerChoice = true;
            vm.state.addAnswerChoiceError = '';
            courseFactory.createAnswerChoice(vm.newAnswerChoice).then(function (choice) {
                vm.state.loadingAddAnswerChoice = false;
                if (!question.answer_choices_detail) {
                    question.answer_choices_detail = [];
                }
                question.answer_choices_detail.push(choice);
                vm.newAnswerChoice = {};
            }, function (error) {
                vm.state.loadingAddAnswerChoice = false;
                console.log(error);
                vm.state.addAnswerChoiceError = 'Problem adding answer choice';
            });
        };

        vm.deleteAnswerChoice = function (question, choice) {
            courseFactory.deleteAnswerChoice(choice).then(function (data) {
                console.log('Deleted');
                var i = _.findIndex(question.answer_choices_detail, function (ac) {
                    return ac.url === choice.url;
                });
                question.answer_choices_detail.splice(i, 1);
                i = _.findIndex(question.answer_choices, function (c) {
                    return c === choice.url;
                });
                question.answer_choices.splice(i, 1);
                if (choice.url === question.correct_single_answer) {
                    question.correct_single_answer = null;
                }
            }, function (err) {
                console.log(err);
            });
        };

        vm.addQuestion = function () {
            vm.state.addQuestionError = '';
            vm.state.addQuestionIsLoading = true;
            vm.newQuestion.quiz = vm.quiz.url;
            // single response only for now; push to an array
            vm.newQuestion.question_type = 'SINGLE_RESPONSE';
            vm.newQuestion.answer_choices = _.map(vm.newQuestion.answer_choices_detail, function (ac) {
                return ac.url;
            });
            console.log(vm.newQuestion);
            courseFactory.createQuizQuestion(vm.newQuestion).then(function (question) {
                vm.questions.push(question);
                vm.quiz.quizquestion_set.push(question);
                $('#add-question-modal').modal('hide');
                vm.newQuestion = {};
                vm.state.addQuestionIsLoading = false;
            }, function (error) {
                vm.state.addQuestionError = 'There was a problem in saving the quiz. Please try again. ' + error;
                vm.state.addQuestionIsLoading = false;
            });
        };

        vm.loadQuestionForEditing = function (question) {
            vm.state.editQuestionError = '';
            vm.selectedQuestion = question;
            $('#edit-question-modal').modal('show');
        };

        vm.editQuestion = function () {
            vm.state.editQuestionIsLoading = true;
            vm.selectedQuestion.answer_choices = _.map(vm.selectedQuestion.answer_choices_detail, function (ac) {
                return ac.url;
            });
            console.log(vm.selectedQuestion);
            courseFactory.updateQuizQuestion(vm.selectedQuestion).then(function (response) {
                vm.selectedQuestion = {};
                vm.state.editQuestionIsLoading = false;
                $('#edit-question-modal').modal('hide');
            }, function (error) {
                vm.state.editQuestionIsLoading = false;
                vm.state.editQuestionError = 'We could not save this question. Please reload the page and try again.';
            });
        };

        vm.loadQuestionForDeletion = function (question) {
            vm.questionToDelete = question;
            $('#delete-question-modal').modal('show');
        };

        vm.deleteQuestion = function () {
            vm.state.deleteQuestionError = '';
            vm.state.deletingQuestion = true;
            courseFactory.deleteQuizQuestion(vm.questionToDelete).then(function (data) {
                vm.state.deletingQuestion = false;
                vm.state.deleteQuestionError = '';
                var i = _.findIndex(vm.quiz.quizquestion_set, function (q) {
                    return q.url === vm.questionToDelete.url;
                });
                vm.quiz.quizquestion_set.splice(i, 1);
                $('#delete-question-modal').modal('hide');
            }, function (error) {
                vm.state.deletingQuestion = false;
                vm.state.deleteQuestionError = 'We could not delete this question. Please reload and try again.';
            });
        };

        vm.showView = function (view) {
            if (view === 'quiz') {
                vm.state.showSubmissions = false;
                vm.state.showQuiz = true;
            } else if (view === 'submissions') {
                vm.state.showSubmissions = true;
                vm.state.showQuiz = false;
            }
        };

        vm.getResponse = function (question, submission) {
            return _.find(submission.quizquestionresponse_set, function (r) {
                return r.quiz_question === question.url;
            });
        };

        function init() {
            courseFactory.getCourse(courseFactory.getCurrentCourseId()).then(function (course) {
                vm.course = course;
                return courseFactory.getInstructorQuiz(courseFactory.getQuizIdFromUrl());
            }).then(function (quiz) {
                vm.quiz = quiz;
                vm.quiz.quizquestion_set = _.sortBy(quiz.quizquestion_set, function (q) {
                    return q.sort_order;
                });
                return courseFactory.getStudents(vm.course.url)
            }).then(function (students) {
                vm.students = students;
                courseFactory.getPastQuizSubmissions(vm.quiz.url).then(function (submissions) {
                    console.log(submissions);
                    _.each(submissions, function (s) {
                        var student = _.find(students, function (st) {
                            return st.user === s.student_user;
                        });
                        if (student.submissions !== undefined) {
                            student.submissions.push(s);
                        } else {
                            student.submissions = [s];
                        }
                    });
                    console.log(students);
                });
            });
        }

        init();
    }
})();
