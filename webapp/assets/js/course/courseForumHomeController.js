'use strict';

(function () {
    angular.module('lms').controller('CourseForumHomeController', ['courseFactory', courseForumHomeController]);

    function courseForumHomeController(courseFactory) {
        var vm = this;
        vm.course = null;
        vm.questions = [];
        vm.newQuestion = {};

        vm.createQuestion = function () {
            vm.newQuestion.course = vm.course.url;
            courseFactory.createQuestion(vm.newQuestion).then(function (question) {
                vm.questions.push(question);
                // then redirect to question page
                vm.newQuestion = {};
                $('#new-question-modal').modal('hide');
            });
        };

        function init() {
            courseFactory.getCourse(courseFactory.getCurrentCourseId()).then(function (course) {
                vm.course = course;
            }).then(function () {
                return courseFactory.getCourseQuestions(vm.course.url);
            }).then(function (questions) {
                vm.questions = questions;
            });
        }

        init();
    }
})();