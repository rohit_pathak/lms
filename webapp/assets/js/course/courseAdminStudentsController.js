'use strict';

(function () {
    angular.module('lms')
        .controller('CourseAdminStudentsController', ['academyFactory', 'courseFactory', StudentsController]);

    function StudentsController(academyFactory, courseFactory) {
        var vm = this;
        vm.course = null;
        vm.academyStudents = [];
        vm.courseStudents = [];
        vm.courseStudentsSet = {};
        vm.selectedStudent = null;
        vm.modalSuccessMessage = '';
        vm.modalErrorMessage = '';
        vm.loading = false;

        vm.addStudent = function (student) {
            vm.modalSuccessMessage = vm.modalErrorMessage = '';
            vm.loading = true;
            if (!(student.url in vm.courseStudentsSet)) {
                var oldStudents = vm.course.students;
                vm.course.students = _.keys(vm.courseStudentsSet);
                vm.course.students.push(student.url);
                courseFactory.updateCourse(vm.course).then(function (course) {
                    vm.loading = false;
                    vm.course = course;
                    vm.courseStudentsSet[student.url] = true;
                    vm.modalSuccessMessage = student.first_name + ' ' + student.last_name + ' was successfully added.'
                    vm.courseStudents.push(student);
                }, function (error) {
                    vm.loading = false;
                    vm.modalErrorMessage = "Couldn't add " + student.first_name + ' ' + student.last_name +
                        '. ' + error.data;
                    vm.course.students = oldStudents;
                });
            }
        };

        vm.removeStudent = function (student) {
            vm.loading = true;
            vm.removeStudentModalError = '';
            vm.course.students = _.filter(vm.course.students, function (s) {
                return s !== student.url;
            });
            courseFactory.updateCourse(vm.course).then(function (response) {
                vm.courseStudents = _.filter(vm.courseStudents, function (s) {
                    return s.url !== student.url;
                });
                vm.selectedStudent = null;
                delete vm.courseStudentsSet[student.url];
                $('#remove-student-modal').modal('hide');
                vm.loading = false;
            }, function (error) {
                vm.course.students = _.map(vm.courseStudents, function (s) {
                    return s.url;
                });
                vm.removeStudentModalError = 'Could not remove student. Please try again.';
                console.log(error);
                vm.loading = false;
            });
        };

        function init() {
            courseFactory.getCourse(courseFactory.getCurrentCourseId()).then(function (course) {
                vm.course = course;
            }).then(function () {
                courseFactory.getStudents(vm.course.url).then(function (students) {
                    vm.courseStudents = students;
                    _.each(vm.courseStudents, function (s) {
                        vm.courseStudentsSet[s.url] = true;
                    });
                });
                academyFactory.getStudents(vm.course.academy).then(function (students) {
                    vm.academyStudents = students;
                    console.log(vm.academyStudents);
                });
            });
        }

        init();
    }
})();
