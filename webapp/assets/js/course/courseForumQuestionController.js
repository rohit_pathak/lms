'use strict';

(function () {
    angular.module('lms').controller('CourseForumQuestionController', ['userFactory', 'courseFactory', courseForumQuestionController]);

    function courseForumQuestionController(userFactory, courseFactory) {
        var vm = this;
        vm.question = null;
        vm.user = null;
        vm.answers = [];
        vm.userAnswer = {};
        vm.alreadyAnswered = true;
        vm.modalErrorMessage = '';
        vm.successMessage = '';
        vm.loading = false;
        vm.newQuestionComment = '';

        vm.answerQuestion = function () {
            vm.loading = true;
            vm.userAnswer.question = vm.question.url;
            courseFactory.answerQuestion(vm.userAnswer).then(function (answer) {
                vm.answers.push(answer);
                $('#add-answer-modal').modal('hide');
                vm.alreadyAnswered = true;
                vm.loading = false;
            }, function (error) {
                vm.modalErrorMessage = "Your answer wasn't submitted. Please try again.";
                console.log(error);
                vm.loading = false;

            });
        };

        vm.editAnswer = function () {
            vm.loading = true;
            courseFactory.updateAnswer(vm.userAnswer).then(function (answer) {
                $('#edit-answer-modal').modal('hide');
                vm.loading = false;
            }, function (error) {
                vm.loading = false;
                console.log(error);
            });
        };

        vm.editQuestion = function () {
            vm.loading = true;
            courseFactory.updateQuestion(vm.question).then(function (question) {
                $('#edit-question-modal').modal('hide');
                vm.loading = false;
            }, function (error) {
                vm.loading = false;
                console.log(error);
            });
        };

        vm.addQuestionComment = function () {
            var comment = {content: vm.newQuestionComment};
            vm.loading = true;
            courseFactory.addQuestionComment(vm.question.url, comment).then(function (data) {
                vm.newQuestionComment = '';
                vm.question.comments.push(data);
                vm.addingQuestionComment = false;
                vm.loading = false;
            });
        };

        vm.addAnswerComment = function (answer) {
            var comment = {content: answer.newComment.content};
            vm.loading = true;
            courseFactory.addAnswerComment(answer.url, comment).then(function (data) {
                answer.newComment.content = '';
                answer.comments.push(data);
                answer.newComment.addingComment = false;
                vm.loading = false;
            });
        };

        function init() {
            courseFactory.getQuestion(courseFactory.getQuestionIdFromUrl()).then(function (question) {
                vm.question = question;
            }).then(function () {
                return courseFactory.getQuestionAnswers(vm.question.url);
            }).then(function (answers) {
                vm.answers = answers;
            }).then(function () {
                userFactory.getUserProfile().then(function (user) {
                    vm.user = user;
                    vm.userAnswer = _.find(vm.answers, function (a) {
                        return a.author.url === user.url
                    });
                    if (vm.userAnswer === undefined) {
                        vm.alreadyAnswered = false;
                        vm.userAnswer = {};
                    }
                });
            });
        }

        init();
    }

})();
