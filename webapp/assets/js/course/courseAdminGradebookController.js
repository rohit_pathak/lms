'use strict';

(function () {
    angular.module('lms')
        .controller('CourseAdminGradebookController', ['courseFactory', GradebookController]);

    function GradebookController(courseFactory) {
        var vm = this;
        vm.course = null;
        vm.students = [];
        vm.studentSet = {};
        vm.topics = [];
        vm.modalSuccessMessage = '';
        vm.modalErrorMessage = '';
        vm.loading = false;
        vm.newTopic = {};
        vm.selectedTopic = null;
        vm.report = {};


        vm.createTopic = function () {
            vm.loading = true;
            vm.newTopic.course = vm.course.url;
            courseFactory.createGrateTopic(vm.newTopic).then(function (topic) {
                vm.topics.push(topic);
                $('#create-topic-modal').modal('hide');
                vm.newTopic = {};
                vm.loading = false;
            }, function (error) {
                console.log(error);
            });
        };

        vm.loadGrades = function (topic) {
            vm.loading = true;
            vm.selectedTopic = topic;
            courseFactory.getGrades(topic.url).then(function (grades) {
                // match existing grades to each student in the course
                _.each(vm.students, function (student) {
                    var studentGrade = _.find(grades, function (grade) {
                        return student.url === grade.student;
                    });
                    if (studentGrade !== undefined) {
                        student.grade = studentGrade;
                        student.gradeState = {'edited': false, 'blank': false, 'loading': false};
                    } else {
                        student.grade = {'student': student.url};
                        student.gradeState = {'edited': false, 'blank': true, 'loading': false};
                    }
                });
                vm.loading = false;
            });
        };

        vm.gradeStudent = function (student) {
            student.gradeState.loading = true;
            if (student.gradeState.blank) {
                student.grade.topic = vm.selectedTopic.url;
                courseFactory.createGrade(student.grade).then(function (grade) {
                    student.grade = grade;
                    student.gradeState.blank = student.gradeState.loading = student.gradeState.edited = student.gradeState.error = false;
                }, function (error) {
                    student.gradeState.loading = false;
                    student.gradeState.error = true;
                });
            } else {
                courseFactory.updateGrade(student.grade).then(function (grade) {
                    student.grade = grade;
                    student.gradeState.loading = student.gradeState.edited = student.gradeState.error = false;
                }, function (error) {
                    student.gradeState.loading = false;
                    student.gradeState.error = true;
                });
            }
        };

        vm.generateReport = function (topic) {
            vm.report.topic = topic;
            vm.report.grades = {};
            if (vm.report.histogramChart !== undefined) vm.report.histogramChart.destroy();
            if (vm.report.letterGradeChart !== undefined) vm.report.letterGradeChart.destroy();
            courseFactory.getGrades(topic.url).then(function (grades) {
                _.each(grades, function (g) {
                    vm.report.grades[g.student] = g;
                });
                var letterGrades = {
                    labels: ['F', 'D', 'D+', 'C', 'C+', 'B', 'B+', 'A', 'A+'],
                    frequencies: []
                };
                _.each(letterGrades.labels, function (l) {
                    letterGrades.frequencies.push(_.filter(grades, function (g) {
                        return g.letter_grade === l;
                    }).length);
                });
                $('#grade-charts').modal('show');
                drawCharts(getHistogram(topic.maximum_marks, _.pluck(grades, 'marks')), letterGrades);
            });
        };

        vm.downloadCSV = function () {
            var lines = 'Student,Marks,Letter Grade,Comments\n';
            _.each(vm.students, function (s) {
                if (vm.report.grades[s.url] === undefined) {
                    lines += s.first_name + ' ' + s.last_name + ',,,\n';
                } else {
                    lines += _.string.join(',', [s.first_name + ' ' + s.last_name, vm.report.grades[s.url].marks || '',
                            vm.report.grades[s.url].letter_grade || '', vm.report.grades[s.url].comments]) + '\n';
                }

            });
            var blob = new Blob([lines], {type: "text/csv"});
            var fileName = _.string.camelize(vm.course.name) + '-' + _.string.camelize(vm.report.topic.name) + '.csv';
            saveAs(blob, fileName);
        };

        function getHistogram(maximum, data) {
            var bins = [];
            var frequencies = [];
            var numIntervals = 10;
            // create bins based on maximum
            if (maximum <= 15) {
                _.each(_.range(maximum), function (n) {
                    bins.push([n, n + 1]);
                });
            } else {
                var interval = Math.round(maximum / numIntervals);
                var start = 0;
                _.each(_.range(numIntervals), function (i) {
                    var end = i + 1 === numIntervals ? maximum : start + interval;
                    bins.push([start, end]);
                    start += interval;
                });
            }
            // calculate frequencies
            _.each(bins, function (b) {
                var count = _.filter(data, function (d) {
                    if (b[1] === maximum) {
                        return d >= b[0] && d <= b[1];
                    }
                    return d >= b[0] && d < b[1];
                }).length;
                frequencies.push(count);
            });
            return {'bins': bins, 'frequencies': frequencies};
        }

        function drawCharts(histogram, letterGrades) {
            var histogramCanvas = $("#histogram"),
                letterGradeCanvas = $('#letter-grade-distribution');
            vm.report.histogramChart = new Chart(histogramCanvas, {
                type: 'bar',
                data: {
                    labels: _.map(histogram.bins, function (b) {
                        return b[0] + '-' + b[1];
                    }),
                    datasets: [{
                        label: 'Number of Students',
                        data: histogram.frequencies
                    }]
                },
                options: {
                    legend: {display: false},
                    scales: {
                        xAxes: [{
                            stacked: true,
                            categoryPercentage: 1.0,
                            scaleLabel: {display: true, labelString: 'Marks'}
                        }],
                        yAxes: [{
                            ticks: {
                                stepSize: 1,
                                max: _.max(histogram.frequencies) + 1 || 1
                            }
                            // scaleLabel: {display: true, labelString: 'Number of students'}
                        }]
                    }
                }
            });
            vm.report.letterGradeChart = new Chart(letterGradeCanvas, {
                type: 'bar',
                data: {
                    labels: letterGrades.labels,
                    datasets: [{
                        label: 'Number of Students',
                        data: letterGrades.frequencies
                    }]
                },
                options: {
                    legend: {display: false},
                    scales: {
                        xAxes: [{
                            categoryPercentage: 0.5,
                            scaleLabel: {
                                display: true,
                                labelString: 'Letter Grade'
                            }
                        }],
                        yAxes: [{
                            ticks: {
                                stepSize: 1,
                                max: _.max(letterGrades.frequencies) + 1 || 1
                            }
                            // scaleLabel: {display: true, labelString: 'Number of students'}
                        }]
                    }
                }
            })
        }

        function init() {
            courseFactory.getCourse(courseFactory.getCurrentCourseId()).then(function (course) {
                vm.course = course;
            }).then(function () {
                return courseFactory.getStudents(vm.course.url).then(function (students) {
                    vm.students = _.sortBy(students, function (student) {
                        return student.last_name;
                    });
                    _.each(vm.students, function (s) {
                        vm.studentSet[s.url] = s;
                    });
                });
            }).then(function () {
                courseFactory.getGradeTopics(vm.course.url).then(function (topics) {
                    vm.topics = topics;
                });
            });
        }

        init();
    }
})();
