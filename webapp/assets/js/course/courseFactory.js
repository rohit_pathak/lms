'use strict';

(function () {
    angular.module('lms').factory('courseFactory', ['$http', courseFactory]);

    function courseFactory(http) {
        var factory = {};

        factory.getCurrentCourseId = function () {
            return parseInt(_.string.strLeft(_.string.strRight(window.location.href, '/courses/'), '/'));
        };

        factory.getCourse = function (courseId) {
            return http.get('/api/courses/' + courseId).then(function (response) {
                return response.data;
            });
        };

        factory.createCourse = function (course) {
            return http.post('/api/courses/', course).then(function (response) {
                return response.data;
            });
        };

        factory.updateCourse = function (course) {
            return http.put(course.url, course).then(function (response) {
                return response.data;
            });
        };

        factory.getInstructors = function (courseUrl) {
            return http.get(courseUrl + 'instructors/').then(function (response) {
                return response.data;
            });
        };

        factory.getStudents = function (courseUrl) {
            return http.get(courseUrl + 'students/').then(function (response) {
                return response.data;
            });
        };

        factory.getDocuments = function (courseUrl) {
            return http.get(courseUrl + 'documents/').then(function (response) {
                return response.data;
            });
        };

        factory.getStudentNotifications = function (courseUrl) {
            return http.get(courseUrl + 'student-notifications/').then(function (response) {
                return response.data;
            });
        };

        factory.getStudentEvents = function (courseUrl) {
            return http.get(courseUrl + 'student-events/').then(function (response) {
                return response.data;
            });
        };

        factory.getResourceTopics = function (courseUrl) {
            return http.get(courseUrl + 'resources/').then(function (response) {
                return response.data;
            });
        };

        factory.getResourceTopicDetails = function (resourceTopicUrl) {
            return http.get(resourceTopicUrl).then(function (response) {
                return response.data;
            });
        };

        factory.createResourceTopic = function (topic) {
            return http.post('/api/courseresources/', topic).then(function (response) {
                return response.data;
            });
        };

        factory.updateResourceTopics = function (courseUrl, resourceTopics) {
            return http.put(courseUrl + 'resources/', resourceTopics).then(function (response) {
                return response.data;
            });
        };

        factory.updateResourceTopic = function (resourceTopic) {
            return http.put(resourceTopic.url, resourceTopic).then(function (response) {
                return response.data;
            });
        };

        factory.deleteResourceTopic = function (topic) {
            return http.delete(topic.url).then(function (response) {
                return response.data;
            });
        };

        factory.getItemDetails = function (item) {
            return http.get(item.url).then(function (response) {
                return response.data;
            });
        };

        factory.createVideoItem = function (item) {
            return http.post('/api/courseresources/videoitems/', item).then(function (response) {
                return response.data;
            });
        };

        factory.createNoteItem = function (item) {
            console.log(item);
            return http.post('/api/courseresources/noteitems/', item).then(function (response) {
                return response.data;
            });
        };

        factory.createLinkItem = function (item) {
            return http.post('/api/courseresources/linkitems/', item).then(function (response) {
                return response.data;
            });
        };

        factory.getInstructorQuiz = function (quizId) {
            return http.get('/api/quizzes/' + quizId).then(function (response) {
                return response.data;
            });
        };

        factory.getStudentQuiz = function (quizId) {
            return http.get('/api/quizzes/student-safe/' + quizId).then(function (response) {
                return response.data;
            });
        };

        factory.createQuiz = function (quiz) {
            return http.post('/api/quizzes/', quiz).then(function (response) {
                return response.data;
            });
        };

        factory.updateQuiz = function (quiz) {
            return http.put(quiz.url, quiz).then(function (response) {
                return response.data;
            });
        };

        factory.createQuizQuestion = function (question) {
            return http.post('/api/quizzes/questions/', question).then(function (response) {
                return response.data;
            });
        };

        factory.updateQuizQuestion = function (question) {
            return http.put(question.url, question).then(function (response) {
                return response.data;
            });
        };

        factory.bulkUpdateQuizQuestions = function (quizUrl, questions) {
            return http.put(quizUrl + 'bulk-update-questions/', questions).then(function (response) {
                return response.data;
            });
            console.log(quizUrl);
            console.log(questions);
        };

        factory.deleteQuizQuestion = function (question) {
            return http.delete(question.url).then(function (response) {
                return response.data;
            });
        };

        factory.createAnswerChoice = function (choice) {
            return http.post('/api/quizzes/answer-choices/', choice).then(function (response) {
                return response.data;
            });
        };

        factory.updateAnswerChoice = function (choice) {
            return http.put(choice.url, choice).then(function (response) {
                return response.data;
            });
        };

        factory.submitQuiz = function (quizUrl, submission) {
            return http.post(quizUrl + 'responses/', submission).then(function (response) {
                return response.data;
            });
        };

        factory.getPastQuizSubmissions = function (quizUrl) {
            return http.get(quizUrl + 'responses/').then(function (response) {
                return response.data;
            });
        };

        factory.deleteAnswerChoice = function (choice) {
            return http.delete(choice.url).then(function (response) {
                return response.data;
            });
        };

        factory.updateTopicItem = function (item) {
            console.log(item);
            return http.put(item.url, item).then(function (response) {
                return response.data;
            });
        };

        factory.updateTopicItems = function (items) {
            if (items.length === 0) return items;
            return http.put(items[0].topic + 'topicitems/', items).then(function (response) {
                return response.data;
            });
        };

        factory.deleteTopicItem = function (item) {
            return http.delete(item.url).then(function (response) {
                return response.data;
            });
        };

        factory.getEmbedUrl = function (url) {
            if (_.string.include(url, '/embed/')) {
                return url
            } else {
                return url.replace('watch?v=', 'embed/')
            }
        };

        // Forum
        factory.getCourseQuestions = function (courseUrl) {
            return http.get(courseUrl + 'questions/').then(function (response) {
                return response.data;
            });
        };

        factory.getQuestionIdFromUrl = function () {
            return parseInt(_.string.strLeft(_.string.strRight(window.location.href, '/forum/questions/'), '/'));
        };

        factory.getQuizIdFromUrl = function () {
            return parseInt(_.string.strLeft(_.string.strRight(window.location.href, '/resources/quizzes/'), '/'));
        };

        factory.getQuestion = function (questionId) {
            return http.get('/api/forum/questions/' + questionId).then(function (response) {
                return response.data;
            });
        };

        factory.createQuestion = function (question) {
            return http.post('/api/forum/questions/', question).then(function (response) {
                return response.data;
            });
        };

        factory.updateQuestion = function (question) {
            return http.put(question.url, question).then(function (response) {
                return response.data;
            })
        };

        factory.getQuestionAnswers = function (questionUrl) {
            return http.get(questionUrl + 'answers/').then(function (response) {
                return response.data;
            });
        };

        factory.addQuestionComment = function (questionUrl, comment) {
            return http.post(questionUrl + 'comments/', comment).then(function (response) {
                return response.data;
            });
        };

        factory.addAnswerComment = function (answerUrl, comment) {
            return http.post(answerUrl + 'comments/', comment).then(function (response) {
                return response.data;
            });
        };

        factory.answerQuestion = function (answer) {
            return http.post('/api/forum/answers/', answer).then(function (response) {
                return response.data;
            });
        };

        factory.updateAnswer = function (answer) {
            return http.put(answer.url, answer).then(function (response) {
                return response.data;
            });
        };

        // Announcements
        factory.getAnnouncement = function (announcementUrl) {
            return http.get(announcementUrl).then(function (response) {
                return response.data;
            });
        };

        factory.getAnnouncements = function (courseUrl) {
            return http.get(courseUrl + 'announcements/').then(function (response) {
                return response.data;
            });
        };

        factory.createAnnouncement = function (announcement) {
            return http.post('/api/announcements/', announcement).then(function (response) {
                return response.data;
            });
        };

        factory.updateAnnouncement = function (announcement) {
            return http.put(announcement.url, announcement).then(function (response) {
                return response.data;
            });
        };

        factory.getGradeTopics = function (courseUrl) {
            return http.get(courseUrl + 'gradetopics/').then(function (response) {
                return response.data;
            });
        };

        factory.createGrateTopic = function (topic) {
            return http.post('/api/gradebook/topics/', topic).then(function (response) {
                return response.data;
            });
        };

        factory.getGrades = function (topicUrl) {
            return http.get(topicUrl + 'grades/').then(function (response) {
                return response.data;
            });
        };

        factory.getStudentGrades = function (courseUrl, studentUrl) {
            var studentId = _.string.strLeft(_.string.strRight(studentUrl, '/students/'), '/');
            return http.get(courseUrl + 'students/' + studentId + '/grades/').then(function (response) {
                return response.data;
            });
        };

        factory.createGrade = function (grade) {
            return http.post('/api/gradebook/grades/', grade).then(function (response) {
                return response.data;
            });
        };

        factory.updateGrade = function (grade) {
            return http.put(grade.url, grade).then(function (response) {
                return response.data;
            });
        };

        factory.getStudentProfile = function (courseId) {
            return http.get('/api/users/current/' + courseId + '/studentprofile/').then(function (response) {
                return response.data;
            });
        };

        return factory;
    }

})();
