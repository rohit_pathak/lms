'use strict';

(function () {
    angular.module('lms').controller('CourseResourcesController', ['courseFactory', courseResourcesController]);

    function courseResourcesController(courseFactory) {
        var vm = this;
        vm.course = null;
        vm.resourceTopics = [];
        vm.baseUrl = window.location.href;

        function init() {
            courseFactory.getCourse(courseFactory.getCurrentCourseId()).then(function (course) {
                vm.course = course;
            }).then(function () {
                return courseFactory.getResourceTopics(vm.course.url);
            }).then(function (resources) {
                // sort topic items for each topic
                _.each(resources, function (t) {
                    t.topicitem_set = _.sortBy(t.topicitem_set, function (i) {
                        return i.sort_order;
                    });
                });
                // now sort the topics themselves
                vm.resourceTopics = _.sortBy(resources, function (t) {
                    return t.sort_order;
                });
            });
        }

        init();

    }

})();
