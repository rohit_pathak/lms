'use strict';

(function () {
    angular.module('lms')
        .controller('CourseAdminResourcesController', ['$cookies', '$window', 'userFactory', 'instructorFactory', 'courseFactory', 'FileUploader', courseAdminTopicsController]);

    function courseAdminTopicsController(cookiesProvider, $window, userFactory, instructorFactory, courseFactory, FileUploader) {
        var vm = this;
        vm.course = null;
        vm.instructor = null;
        vm.topics = null;
        vm.newTopic = {};
        vm.selectedTopic = {};
        vm.newVideoItem = {};
        vm.newNoteItem = {};
        vm.newLinkItem = {};
        vm.newQuiz = {};
        vm.selectedVideoItem = null;
        vm.selectedNoteItem = null;
        vm.selectedLinkItem = null;
        vm.selectedQuiz = null;
        vm.itemToDelete = null;
        vm.courseDocuments = [];
        vm.uploader = null;
        vm.loading = false;
        vm.modalErrorMessage = '';

        vm.topicSortableOptions = {
            handle: '> .topic-heading .topic-sort-handle',
            stop: function (e, ui) {
                // update sort_orders in displayed topics
                var i = 0;
                _.each(vm.topics, function (t) {
                    t.sort_order = i;
                    i += 1;
                });
                // now update sort orders on the backend
                var updatedTopics = _.map(vm.topics, function (t) {
                    return {
                        course: t.course,
                        name: t.name,
                        id: t.id,
                        url: t.url,
                        sort_order: t.sort_order
                    }
                });
                courseFactory.updateResourceTopics(vm.course.url, updatedTopics); // ignore request result
            }
        };

        vm.itemSetBeingSorted = null;
        vm.topicItemSortableOptions = {
            handle: '> .control-buttons .topic-item-sort-handle',
            update: function (e, ui) {
                vm.itemSetBeingSorted = ui.item.sortable.droptargetModel;
            },
            stop: function (e, ui) {
                var i = 0;
                _.each(vm.itemSetBeingSorted, function (item) {
                    item.sort_order = i;
                    i += 1;
                });
                courseFactory.updateTopicItems(vm.itemSetBeingSorted);
            }
        };

        vm.createTopic = function (topic) {
            vm.modalErrorMessage = '';
            vm.loading = true;
            vm.newTopic.course = vm.course.url;
            vm.newTopic.sort_order = vm.topics.length;
            courseFactory.createResourceTopic(vm.newTopic).then(function (topic) {
                if (!('topicitem_set' in topic)) {
                    topic.topicitem_set = [];
                }
                vm.topics.push(topic);
                vm.newTopic = {};
                $('#new-topic-modal').modal('hide');
                vm.loading = false;
            }, function (error) {
                vm.loading = false;
                vm.modalErrorMessage = 'There was a problem in saving this topic. Please refresh this page are try again.'
            });
        };

        vm.updateTopic = function (topic) {
            vm.modalErrorMessage = '';
            vm.loading = true;
            courseFactory.updateResourceTopic(vm.selectedTopic).then(function (topic) {
                vm.selectedTopic = {};
                $('#edit-topic-modal').modal('hide');
                vm.loading = false;
            }, function (error) {
                vm.modalErrorMessage = 'There was a problem in saving this topic. Please refresh this page are try again.'
                vm.loading = false;
            });
        };

        vm.deleteTopic = function (topic) {
            vm.loading = true;
            vm.modalErrorMessage = '';
            courseFactory.deleteResourceTopic(vm.selectedTopic).then(function (response) {
                vm.topics = _.filter(vm.topics, function (t) {
                    return t.id !== vm.selectedTopic.id;
                });
                vm.selectedTopic = {};
                $('#delete-topic-modal').modal('hide');
                vm.loading = false;
            }, function (error) {
                console.log(error);
                vm.loading = false;
            });
        };

        vm.createVideoItem = function () {
            vm.loading = true;
            vm.modalErrorMessage = '';
            vm.newVideoItem.topic = vm.selectedTopic.url;
            vm.newVideoItem.sort_order = vm.selectedTopic.topicitem_set.length;
            courseFactory.createVideoItem(vm.newVideoItem).then(function (item) {
                vm.selectedTopic.topicitem_set.push(item);
                $('#create-video-item-modal').modal('hide');
                vm.newVideoItem = {};
                vm.loading = false;
            }, function (error) {
                vm.loading = false;
                vm.modalErrorMessage = 'There was a problem in saving this video. Please refresh this page are try again.'
            });
        };

        vm.updateVideoItem = function () {
            vm.loading = true;
            vm.modalErrorMessage = '';
            courseFactory.updateTopicItem(vm.selectedVideoItem).then(function (item) {
                $('#edit-video-item-modal').modal('hide');
                vm.selectedVideoItem = null;
                vm.loading = false;
            }, function (error) {
                vm.loading = false;
                vm.modalErrorMessage = 'There was a problem in saving this video. Please refresh this page are try again.'
            });
        };

        vm.createNoteItem = function () {
            vm.loading = true;
            vm.modalErrorMessage = '';
            vm.newNoteItem.topic = vm.selectedTopic.url;
            vm.newNoteItem.sort_order = vm.selectedTopic.topicitem_set.length;
            courseFactory.createNoteItem(vm.newNoteItem).then(function (item) {
                vm.selectedTopic.topicitem_set.push(item);
                $('#create-note-item-modal').modal('hide');
                vm.newNoteItem = {};
                vm.loading = false;
            }, function (error) {
                vm.loading = false;
                vm.modalErrorMessage = 'There was a problem in saving this note. Please refresh this page are try again.'
            });
        };

        vm.updateNoteItem = function () {
            vm.modalErrorMessage = '';
            courseFactory.updateTopicItem(vm.selectedNoteItem).then(function (item) {
                $('#edit-note-item-modal').modal('hide');
                vm.selectedNoteItem = null;
                vm.loading = false;
            }, function (error) {
                vm.loading = false;
                vm.modalErrorMessage = 'There was a problem in saving this note. Please refresh this page are try again.'
            });
        };

        vm.toggleDoc = function (note, document) {
            if (!('documents' in note)) {
                note.documents = [];
            }
            var index = _.findIndex(note.documents, function (d) {
                return d === document.url;
            });
            if (index === -1) {
                note.documents.push(document.url);
            } else {
                note.documents.splice(index, 1);
            }
        };

        vm.createLinkItem = function () {
            vm.loading = true;
            vm.modalErrorMessage = '';
            vm.newLinkItem.topic = vm.selectedTopic.url;
            vm.newLinkItem.sort_order = vm.selectedTopic.topicitem_set.length;
            courseFactory.createLinkItem(vm.newLinkItem).then(function (item) {
                vm.selectedTopic.topicitem_set.push(item);
                $('#create-link-item-modal').modal('hide');
                vm.newLinkItem = {};
                vm.loading = false;
            }, function (error) {
                vm.loading = false;
                vm.modalErrorMessage = 'There was a problem in saving this link. Please refresh this page are try again.'
            });
        };

        vm.updateLinkItem = function () {
            vm.loading = true;
            vm.modalErrorMessage = '';
            courseFactory.updateTopicItem(vm.selectedLinkItem).then(function (item) {
                $('#edit-link-item-modal').modal('hide');
                vm.selectedLinkItem = null;
                vm.loading = false;
            }, function (error) {
                vm.loading = false;
                vm.modalErrorMessage = 'There was a problem in saving this video. Please refresh this page are try again.'
            });
        };

        vm.createQuiz = function () {
            vm.loading = true;
            vm.modalErrorMessage = '';
            vm.newQuiz.topic = vm.selectedTopic.url;
            vm.newQuiz.sort_order = vm.selectedTopic.topicitem_set.length;
            courseFactory.createQuiz(vm.newQuiz).then(function (item) {
                vm.selectedTopic.topicitem_set.push(item);
                $('#create-quiz-modal').modal('hide');
                vm.newQuiz = {};
                vm.loading = false;
                $window.location.href = vm.getQuizAdminPageUrl(item.id);
            }, function (error) {
                vm.loading = false;
                vm.modalErrorMessage = 'There was a problem in saving this quiz. Please refresh this page are try again.'
            });
        };

        vm.editTopicItemClick = function (item) {
            courseFactory.getItemDetails(item).then(function (details) {
                _.extend(item, details);
                if (item.category === 'video') {
                    vm.selectedVideoItem = item;
                    $('#edit-video-item-modal').modal('show');
                } else if (item.category === 'note') {
                    vm.selectedNoteItem = item;
                    $('#edit-note-item-modal').modal('show');
                } else if (item.category === 'link') {
                    vm.selectedLinkItem = item;
                    $('#edit-link-item-modal').modal('show');
                }
            });
        };

        vm.deleteTopicItem = function () {
            courseFactory.deleteTopicItem(vm.itemToDelete).then(function (res) {
                var topic = _.find(vm.topics, function (t) {
                    return t.url === vm.itemToDelete.topic;
                });
                topic.topicitem_set = _.filter(topic.topicitem_set, function (i) {
                    return i.id !== vm.itemToDelete.id;
                });
                vm.itemToDelete = null;
                $('#delete-topic-item-modal').modal('hide');
            }, function (res) {
                console.log(res);
            });
        };

        vm.getEmbedUrl = function (url) {
            return courseFactory.getEmbedUrl(url);
        };

        vm.getQuizAdminPageUrl = function (quizId) {
            return _.string.endsWith($window.location.pathname, '/') ? $window.location.pathname + 'quizzes/' + quizId : $window.location.pathname + '/quizzes/' + quizId;
        };

        function initializeFileUploader() {
            vm.uploader = new FileUploader({
                headers: {
                    'X-CSRFToken': cookiesProvider.get('csrftoken')
                },
                url: vm.course.url + 'documents/'
            });
            vm.uploader.onAfterAddingFile = function (fileItem) {
                fileItem.upload();
            };
            vm.uploader.onSuccessItem = function (fileItem, response, status, headers) {
                courseFactory.getDocuments(vm.course.url).then(function (documents) {
                    vm.courseDocuments = documents;
                });
            };
            vm.uploader.onErrorItem = function (fileItem, response, status, headers) {
                console.info('onErrorItem file', fileItem, response, status, headers);
            };
        }

        function init() {
            courseFactory.getCourse(courseFactory.getCurrentCourseId()).then(function (course) {
                vm.course = course;
            }).then(function () {
                userFactory.getUserProfile().then(function (user) {
                    courseFactory.getInstructors(vm.course.url).then(function (instructors) {
                        vm.instructor = _.find(instructors, function (i) {
                            return i.user === user.url;
                        });
                        initializeFileUploader();
                    }).then(function () {
                        courseFactory.getDocuments(vm.course.url).then(function (documents) {
                            vm.courseDocuments = documents;
                        });
                    });
                });
                return courseFactory.getResourceTopics(vm.course.url);
            }).then(function (topics) {
                vm.topics = _.sortBy(topics, function (t) {
                    return t.sort_order;
                });
                // sort topic items by their sort order too
                _.each(vm.topics, function (t) {
                    t.topicitem_set = _.sortBy(t.topicitem_set, function (i) {
                        return i.sort_order;
                    });
                });
            });
        }

        init();
    }
})();