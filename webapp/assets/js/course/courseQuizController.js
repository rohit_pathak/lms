;'use strict';

(function () {
    angular.module('lms').controller('CourseQuizController', ['courseFactory', courseQuizController]);

    function courseQuizController(courseFactory) {
        var vm = this;
        vm.course = null;
        vm.quiz = null;
        vm.latestSubmission = null;
        vm.pastSubmissions = [];
        vm.showPastSubmissions = false;
        vm.showLatestSubmission = false;
        vm.showFeedback = false;


        vm.submitQuiz = function () {
            vm.showLatestSubmission = true;
            vm.latestSubmissionLoading = true;
            var submission = {
                quizquestionresponse_set: _.chain(vm.quiz.quizquestion_set)
                    .filter(function (q) {
                        return q.selectedSingleAnswer;
                    }).map(function (q) {
                        return {quiz_question: q.url, single_answer_response: q.selectedSingleAnswer};
                    })
            };
            courseFactory.submitQuiz(vm.quiz.url, submission).then(function (submission) {
                vm.latestSubmissionLoading = false;
                vm.pastSubmissions.push(submission);
                vm.latestSubmission = submission;
            }, function (error) {
                console.log(error)
            });
        };

        vm.getResponse = function (question, submission) {
            return _.find(submission.quizquestionresponse_set, function (r) {
                return r.quiz_question === question.url;
            });
        };

        vm.reloadQuiz = function () {
            _.each(vm.quiz.quizquestion_set, function (q) {
                q.selectedSingleAnswer = null;
            });
            vm.showPastSubmissions = false;
            vm.showLatestSubmission = false;
        };

        function getQuizId() {
            return parseInt(_.string.strLeft(_.string.strRight(window.location.href, '/items/'), '/'));
        }

        function init() {
            courseFactory.getStudentQuiz(getQuizId()).then(function (quiz) {
                vm.quiz = quiz;
                return courseFactory.getPastQuizSubmissions(quiz.url);
            }).then(function (submissions) {
                vm.pastSubmissions = submissions;
            });
        }

        init();

    }
})();