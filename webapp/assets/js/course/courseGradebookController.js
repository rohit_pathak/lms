'use strict';

(function () {
    angular.module('lms')
        .controller('CourseGradebookController', ['courseFactory', GradebookController]);

    function GradebookController(courseFactory) {
        var vm = this;
        vm.course = null;
        vm.student = null;
        vm.topics = [];
        vm.grades = [];

        function init() {
            courseFactory.getCourse(courseFactory.getCurrentCourseId()).then(function (course) {
                vm.course = course;
            }).then(function () {
                return courseFactory.getStudentProfile(vm.course.id).then(function (student) {
                    vm.student = student;
                });
            }).then(function () {
                return courseFactory.getGradeTopics(vm.course.url).then(function (topics) {
                    vm.topics = topics;
                });
            }).then(function () {
                courseFactory.getStudentGrades(vm.course.url, vm.student.url).then(function (grades) {
                    _.each(vm.topics, function (topic) {
                        var grade = _.find(grades, function (g) {
                            return g.topic === topic.url;
                        });
                        topic.grade = grade ? grade : {};
                    });
                });
            });
        }

        init();
    }
})();
