'use strict';

(function () {
    angular.module('lms')
        .controller('CourseHomeController', ['courseFactory', HomeController]);

    function HomeController(courseFactory) {
        var vm = this;
        vm.course = null;
        vm.events = [];

        function init() {
            courseFactory.getCourse(courseFactory.getCurrentCourseId()).then(function (course) {
                vm.course = course;
            }).then(function () {
                return courseFactory.getStudentEvents(vm.course.url).then(function (events) {
                    console.log(events);
                    vm.events = events;
                });
            });
        }

        init();
    }
})();
