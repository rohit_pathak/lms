'use strict';

(function () {
    angular.module('lms')
        .controller('CourseAdminAnnouncementsController', ['courseFactory', AdminAnnouncementsController]);

    function AdminAnnouncementsController(courseFactory) {
        var vm = this;
        vm.course = null;
        vm.announcements = [];
        vm.newAnnouncement = {};
        vm.selectedAnnouncement = null;
        vm.createErrorMessage = '';
        vm.updateErrorMessage = '';
        vm.savingAnnouncement = false;

        vm.createAnnouncement = function () {
            vm.savingAnnouncement = true;
            vm.createErrorMessage = '';
            vm.newAnnouncement.course = vm.course.url;
            courseFactory.createAnnouncement(vm.newAnnouncement).then(function (announcement) {
                vm.announcements.push(announcement);
                vm.newAnnouncement = {};
                vm.savingAnnouncement = false;
                $('#new-announcement-modal').modal('hide');
            }, function (error) {
                vm.createErrorMessage = "There was some problem creating this announcement. Please try again.";
                vm.savingAnnouncement = false;
                console.error(error);
            });
        };

        vm.loadAnnouncement = function (announcement) {
            courseFactory.getAnnouncement(announcement.url).then(function (result) {
                vm.selectedAnnouncement = result;
            }, function (error) {
                console.error('failed to load announcement');
                console.error(error);
            });
        };

        vm.updateAnnouncement = function () {
            vm.savingAnnouncement = true;
            vm.updateErrorMessage = '';
            courseFactory.updateAnnouncement(vm.selectedAnnouncement).then(function (result) {
                var i = _.findIndex(vm.announcements, function (a) {
                    return a.url === result.url;
                });
                vm.announcements[i] = result;
                vm.savingAnnouncement = false;
                $('#edit-announcement-modal').modal('hide');
            }, function (error) {
                console.error(error);
                vm.savingAnnouncement = false;
                vm.updateErrorMessage = 'There was a problem saving the announcement. Please reload and try again.';
            });
        };

        function init() {
            courseFactory.getCourse(courseFactory.getCurrentCourseId()).then(function (course) {
                vm.course = course;
            }).then(function () {
                return courseFactory.getAnnouncements(vm.course.url).then(function (announcements) {
                    vm.announcements = announcements;
                });
            });
        }

        init();
    }
})();
