;'use strict';

(function () {
    angular.module('lms').factory('userFactory', ['$http', userFactory]);

    function userFactory(http) {
        var factory = {};

        factory.getUserProfile = function () {
            return http.get('/api/users/current/').then(function (response) {
                return response.data;
            });
        };
        return factory;
    }
})();