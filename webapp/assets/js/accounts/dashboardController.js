'use strict';

(function () {
    angular.module('lms')
        .controller('DashboardController', ['$http', DashboardController]);

    function DashboardController(http) {
        var vm = this;
        this.studentCourses = [];
        this.instructorCourses = [];
        http.get('/api/users/current/courses/').then(function (response) {
            vm.studentCourses = response.data.student;
            vm.instructorCourses = response.data.instructor;
        });
    }
})();
