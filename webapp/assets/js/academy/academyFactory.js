'use strict';

(function () {
    angular.module('lms').factory('academyFactory', ['$http', academyFactory]);

    function academyFactory(http) {
        var factory = {};

        factory.getCurrentAcademyShortName = function () {
            return _.string.strLeft(_.string.strRight(window.location.href, '/academy/'), '/');
        };

        factory.getAcademy = function (shortName) {
            return http.get('/api/academies/' + shortName).then(function (response) {
                return response.data;
            });
        };

        factory.getInstructors = function (academyUrl) {
            return http.get(academyUrl + 'instructors/').then(function (response) {
                return response.data;
            });
        };

        factory.createInstructor = function (instructor) {
            return http.post('/api/instructors/', instructor).then(function (response) {
                return response.data;
            });
        };

        factory.getStudents = function (academyUrl) {
            return http.get(academyUrl + 'students/').then(function (response) {
                return response.data;
            });
        };

        factory.getStudentDetail = function (studentUrl) {
            return http.get(studentUrl).then(function (response) {
                return response.data;
            });
        };

        factory.createStudent = function (student) {
            return http.post('/api/students/', student).then(function (response) {
                return response.data;
            });
        };

        factory.updateStudent = function (student) {
            return http.put(student.url, student).then(function (response) {
                return response.data;
            });
        };

        factory.addStudents = function (academyUrl, emails) {
            return http.post(academyUrl + 'add-students/', {'emails': emails}).then(function (response) {
                return response.data;
            });
        };

        factory.getInvitedStudents = function (academyUrl) {
            return http.get(academyUrl + 'student-invitations/').then(function (response) {
                return response.data;
            });
        };

        factory.getCourses = function (academyUrl) {
            return http.get(academyUrl + 'courses/').then(function (response) {
                return response.data;
            });
        };

        factory.queryUserByEmail = function (email) {
            return http.post('/api/users/queryemail/', {'email': email}).then(function (response) {
                return response.data;
            });
        };

        return factory;
    }

})();
