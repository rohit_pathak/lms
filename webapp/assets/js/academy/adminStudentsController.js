'use strict';

(function () {
    angular.module('lms')
        .controller('AdminStudentsController', ['academyFactory', DashboardController]);

    function DashboardController(academyFactory) {
        var vm = this;
        vm.academy = null;
        vm.students = [];
        vm.newStudent = {};
        vm.selectedStudent = {};
        vm.errorMessage = '';
        vm.emails = '';
        vm.addResult = {};
        vm.invitations = [];

        vm.createStudent = function () {
            vm.errorMessage = '';
            vm.newStudent.academy = vm.academy.url;
            vm.is_active = true;
            academyFactory.queryUserByEmail(vm.newStudent.email).then(function (ref) {
                if (ref === '') {
                    vm.errorMessage = 'This email does not exist in our database.';
                } else {
                    vm.newStudent.user = ref;
                    academyFactory.createStudent(vm.newStudent).then(function (student) {
                        vm.errorMessage = '';
                        $('#create-student-modal').modal('hide');
                        vm.students.push(student);
                        vm.newStudent = {};
                    }, function (error) {
                        console.log(error);
                        vm.errorMessage = _.string.join.apply(this, _.union([', '], error.data.non_field_errors));
                    });
                }
            });
        };

        vm.addStudents = function () {
            vm.errorMessage = '';
            academyFactory.addStudents(vm.academy.url, vm.emails).then(function (result) {
                vm.addResult = result;
                vm.emails = '';
                $('#add-students-modal').modal('hide');
                $('#results-modal').modal('show');
                init();
            }, function (error) {
                vm.errorMessage = 'There was a problem with adding students. Please try again later.';
                console.log(error);
            });
        };

        vm.loadStudent = function (student) {
            vm.loading = true;
            academyFactory.getStudentDetail(student.url).then(function (data) {
                vm.selectedStudent = data;
                vm.loading = false;
            }, function (error) {
                vm.modalErrorMessage = 'Could not retrieve student details.';
                vm.loading = false;
                console.log(error);
            });
        };

        vm.editStudent = function (student) {
            academyFactory.updateStudent(student).then(function (data) {
                var updatedStudent = _.find(vm.students, function (s) {
                    return s.url === data.url;
                });
                updatedStudent.identification = data.identification;
                $('#edit-student-modal').modal('hide');
            }, function (error) {
                console.log(error);
            });
        };

        vm.removeStudent = function (student) {
            student.is_active = false;
            academyFactory.updateStudent(student).then(function (data) {
                var index = _.findIndex(vm.students, function (s) {
                    return s.url === student.url;
                });
                vm.students.splice(index, 1);
                $('#remove-student-modal').modal('hide');
            }, function (error) {
                console.log(error);
            });
        };

        function init() {
            academyFactory.getAcademy(academyFactory.getCurrentAcademyShortName()).then(function (academy) {
                vm.academy = academy;
            }).then(function () {
                academyFactory.getStudents(vm.academy.url).then(function (students) {
                    vm.students = students;
                });
                academyFactory.getInvitedStudents(vm.academy.url).then(function (invitations) {
                    vm.invitations = invitations;
                });
            });
        }

        init();
    }
})();
