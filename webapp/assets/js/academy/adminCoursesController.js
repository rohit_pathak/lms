'use strict';

(function () {
    angular.module('lms')
        .controller('AdminCoursesController', ['academyFactory', 'courseFactory', DashboardController]);

    function DashboardController(academyFactory, courseFactory) {
        var vm = this;
        vm.academy = null;
        vm.courses = [];
        vm.instructors = [];
        vm.newCourse = {};
        vm.selectedCourse = {};
        vm.updateErrorMessage = null;

        vm.createCourse = function () {
            vm.newCourse.academy = vm.academy.url;
            courseFactory.createCourse(vm.newCourse).then(function (course) {
                vm.courses.push(course);
                $('#create-course-modal').modal('hide');
                vm.newCourse = {};
            }, function (error) {
                console.log(error);
            });
        };

        vm.loadCourse = function (course) {
            vm.updateErrorMessage = null;
            courseFactory.getCourse(course.id).then(function (response) {
                vm.selectedCourse = response;
            }, function (error) {
                console.log(error);
                vm.updateErrorMessage = 'Course not load course details. Please refresh and try again.';
            });
        };

        vm.updateCourse = function (course) {
            vm.updateErrorMessage = null;
            courseFactory.updateCourse(course).then(function (response) {
                var oldCourseIndex = _.findIndex(vm.courses, function (c) {
                    return c.id === response.id;
                });
                vm.courses[oldCourseIndex] = response;
                $('#edit-course-modal').modal('hide');
            }, function (error) {
                vm.updateErrorMessage = "Problems encountered while saving course. Please refresh and try again.";
                console.log(error);
            });
        };

        function init() {
            academyFactory.getAcademy(academyFactory.getCurrentAcademyShortName()).then(function (academy) {
                vm.academy = academy;
            }).then(function () {
                academyFactory.getInstructors(vm.academy.url).then(function (instructors) {
                    vm.instructors = instructors;
                });
                academyFactory.getCourses(vm.academy.url).then(function (courses) {
                    vm.courses = courses;
                });
            });
        }

        init();
    }
})();
