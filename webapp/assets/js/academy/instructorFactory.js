;'use strict';

(function () {
    angular.module('lms').factory('instructorFactory', ['$http', instructorFactory]);

    function instructorFactory(http) {
        var factory = {};

        factory.getInstructorDocuments = function (instructorUrl) {
            return http.get(instructorUrl + 'documents/').then(function (response) {
                return response.data;
            });
        };

        return factory;
    }
})();