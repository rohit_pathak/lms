'use strict';

(function () {
    angular.module('lms')
        .controller('AdminInstructorsController', ['academyFactory', DashboardController]);

    function DashboardController(academyFactory) {
        var vm = this;
        vm.academy = null;
        vm.instructors = [];
        vm.newInstructor = {};
        vm.errorMessage = '';

        vm.createInstructor = function () {
            vm.errorMessage = '';
            vm.newInstructor.academy = vm.academy.url;
            vm.is_active = true;
            academyFactory.queryUserByEmail(vm.newInstructor.email).then(function (ref) {
                if (ref === '') {
                    vm.errorMessage = 'This email does not exist in our database.';
                } else {
                    vm.newInstructor.user = ref;
                    academyFactory.createInstructor(vm.newInstructor).then(function (instructor) {
                        vm.errorMessage = '';
                        $('#create-instructor-modal').modal('hide');
                        vm.instructors.push(instructor);
                        vm.newInstructor = {};
                    }, function (error) {
                        console.log(error);
                        vm.errorMessage = _.string.join.apply(this, _.union([', '], error.data.non_field_errors));
                    });
                }
            });
        };

        function init() {
            academyFactory.getAcademy(academyFactory.getCurrentAcademyShortName()).then(function (academy) {
                vm.academy = academy;
            }).then(function () {
                academyFactory.getInstructors(vm.academy.url).then(function (instructors) {
                    vm.instructors = instructors;
                });
            });
        }

        init();
    }
})();
