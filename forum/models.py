from __future__ import unicode_literals

from django.conf import settings
from django.db import models

from comment.models import Comment
from course.models import Course
from student.models import Student


class Question(models.Model):
    course = models.ForeignKey(Course)
    author = models.ForeignKey(settings.AUTH_USER_MODEL)
    title = models.CharField(max_length=200)
    text = models.TextField()
    votes = models.ManyToManyField(settings.AUTH_USER_MODEL, related_name='questions_upvoted', blank=True)
    comments = models.ManyToManyField(Comment)
    created_timestamp = models.DateTimeField(auto_now_add=True)
    modified_timestamp = models.DateTimeField(auto_now=True)

    @property
    def answer_count(self):
        return self.answer_set.count()

    @property
    def vote_count(self):
        return self.votes.all().count()

    def __unicode__(self):
        return '{} | {}'.format(self.title, self.author)

    class Meta:
        unique_together = ('course', 'title')


class Answer(models.Model):
    question = models.ForeignKey(Question)
    author = models.ForeignKey(settings.AUTH_USER_MODEL)
    text = models.TextField()
    votes = models.ManyToManyField(settings.AUTH_USER_MODEL, related_name='answers_upvoted', blank=True)
    comments = models.ManyToManyField(Comment)
    created_timestamp = models.DateTimeField(auto_now_add=True)
    modified_timestamp = models.DateTimeField(auto_now=True)

    @property
    def vote_count(self):
        return self.votes.all().count()

    def __unicode__(self):
        return '{} | {}, {}'.format(self.text[:40], self.author, self.question.pk)

    class Meta:
        unique_together = ('author', 'question')
