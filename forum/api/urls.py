from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns

from forum.api import views

urlpatterns = [
    url(r'^questions/$', views.QuestionList.as_view(), name='question-list'),
    url(r'^questions/(?P<pk>\d+)/$', views.QuestionDetail.as_view(), name='question-detail'),
    url(r'^questions/(?P<pk>\d+)/answers/$', views.QuestionAnswers.as_view(), name='question-answers'),
    url(r'^questions/(?P<pk>\d+)/comments/$', views.QuestionComments.as_view(), name='question-comments'),
    # url(r'^questions/(?P<pk>\d+)/upvote/$', views.UpvoteQuestion.as_view(), name='upvote-question'),
    url(r'^answers/$', views.AnswerList.as_view(), name='answer-list'),
    url(r'^answers/(?P<pk>\d+)/$', views.AnswerDetail.as_view(), name='answer-detail'),
    url(r'^answers/(?P<pk>\d+)/comments/$', views.AnswerComments.as_view(), name='answer-comments'),
    # url(r'^answers/(?P<pk>\d+)/upvote/$', views.UpvoteAnswer.as_view(), name='upvote-answer'),
]

urlpatterns = format_suffix_patterns(urlpatterns)
