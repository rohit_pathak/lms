import textwrap

from django.core.mail import EmailMessage
from django.core.urlresolvers import reverse
from django.shortcuts import get_object_or_404
from rest_framework import generics
from rest_framework.permissions import IsAuthenticated

import constants
from comment.api.serializers import CommentSerializer
from course.api.permissions import course_member_or_forbidden
from forum.api.permissions import author_or_forbidden
from forum.api.serializers import QuestionSerializer, AnswerSerializer
from forum.models import Question, Answer


class QuestionList(generics.CreateAPIView):
    serializer_class = QuestionSerializer
    permission_classes = (IsAuthenticated,)
    queryset = Question.objects.all()

    def perform_create(self, serializer):
        course_member_or_forbidden(self.request.user, serializer.validated_data.get('course'))
        question = serializer.save(author=self.request.user)
        email_subject = '%s: New question' % (question.course.name,)
        # TODO: textwrap.dedent()
        email_body = """
        %s
        %s

        Hello,

        A course member asked the following question on the forum:
        "%s"
        Read full details at %s.

        Thanks,
        The Coursetrack Team
        """
        s_body = email_body % (question.course.name, question.course.academy.name, question.title,
                               self.request.build_absolute_uri(reverse('course-forum-question-detail-page',
                                                                       args=(question.course.pk, question.pk))))
        students_email = EmailMessage(subject=email_subject, body=textwrap.dedent(s_body),
                                      to=[constants.UNDISCLOSED_RECIPIENTS],
                                      bcc=[s.user.email for s in question.course.students.all()])
        students_email.send(fail_silently=True)
        i_body = email_body % (question.course.name, question.course.academy.name, question.title,
                               self.request.build_absolute_uri(reverse('course-admin-forum-question-detail-page',
                                                                       args=(question.course.pk, question.pk))))
        instructors_email = EmailMessage(subject=email_subject, body=textwrap.dedent(i_body),
                                         to=[constants.UNDISCLOSED_RECIPIENTS],
                                         bcc=[i.user.email for i in question.course.instructors.all()])
        instructors_email.send(fail_silently=True)


class QuestionDetail(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = QuestionSerializer
    permission_classes = (IsAuthenticated,)
    queryset = Question.objects.all()

    def get(self, request, *args, **kwargs):
        course_member_or_forbidden(self.request.user, self.get_object().course)
        return self.retrieve(request, *args, **kwargs)

    def perform_update(self, serializer):
        author_or_forbidden(self.get_object(), self.request.user)
        serializer.save()

    def perform_destroy(self, instance):
        author_or_forbidden(self.get_object(), self.request.user)
        instance.delete()


class AnswerList(generics.CreateAPIView):
    serializer_class = AnswerSerializer
    permission_classes = (IsAuthenticated,)
    queryset = Answer.objects.all()

    def perform_create(self, serializer):
        course_member_or_forbidden(self.request.user, serializer.validated_data.get('question').course)
        serializer.save(author=self.request.user)


class AnswerDetail(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = AnswerSerializer
    permission_classes = (IsAuthenticated,)
    queryset = Answer.objects.all()

    def get(self, request, *args, **kwargs):
        course_member_or_forbidden(self.request.user, self.get_object().question.course)
        return self.retrieve(request, *args, **kwargs)

    def perform_update(self, serializer):
        author_or_forbidden(self.get_object(), self.request.user)
        serializer.save()

    def perform_destroy(self, instance):
        author_or_forbidden(self.get_object(), self.request.user)
        instance.delete()


class AnswerComments(generics.ListCreateAPIView):
    serializer_class = CommentSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        answer = get_object_or_404(Answer, pk=self.kwargs['pk'])
        course_member_or_forbidden(self.request.user, answer.question.course)
        return answer.comments.all()

    def perform_create(self, serializer):
        answer = get_object_or_404(Answer, pk=self.kwargs['pk'])
        course_member_or_forbidden(self.request.user, answer.question.course)
        comment = serializer.save(author=self.request.user)
        answer.comments.add(comment)
        answer.save()


class QuestionAnswers(generics.ListAPIView):
    serializer_class = AnswerSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        question = get_object_or_404(Question, pk=self.kwargs['pk'])
        course_member_or_forbidden(self.request.user, question.course)
        return question.answer_set.all()


class QuestionComments(generics.ListCreateAPIView):
    # Note to self: This is awesome. Nested views can be created pretty easily like the following.
    # Use this as a template for future nested views (until you find something cooler)
    serializer_class = CommentSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        question = get_object_or_404(Question, pk=self.kwargs['pk'])
        course_member_or_forbidden(self.request.user, question.course)
        return question.comments.all()

    def perform_create(self, serializer):
        question = get_object_or_404(Question, pk=self.kwargs['pk'])
        course_member_or_forbidden(self.request.user, question.course)
        comment = serializer.save(author=self.request.user)
        question.comments.add(comment)
        question.save()
