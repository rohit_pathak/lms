from rest_framework import serializers

from accounts.api.serializers import UserSerializer
from comment.api.serializers import CommentSerializer
from forum.models import Question, Answer


class QuestionRef(serializers.HyperlinkedModelSerializer):
    author = UserSerializer(read_only=True)

    class Meta:
        model = Question
        fields = ('id', 'url', 'course', 'author', 'title', 'vote_count', 'answer_count', 'created_timestamp',
                  'modified_timestamp')
        read_only_fields = ('author', 'created_timestamp', 'modified_timestamp')


class QuestionSerializer(serializers.HyperlinkedModelSerializer):
    author = UserSerializer(read_only=True)
    comments = CommentSerializer(read_only=True, many=True)

    def update(self, instance, validated_data):
        instance.title = validated_data.get('title')
        instance.text = validated_data.get('text')
        instance.save()
        return instance

    class Meta:
        model = Question
        fields = ('id', 'url', 'course', 'author', 'title', 'text', 'vote_count', 'comments',
                  'created_timestamp', 'modified_timestamp')
        read_only_fields = ('author', 'created_timestamp', 'modified_timestamp')


class AnswerSerializer(serializers.HyperlinkedModelSerializer):
    author = UserSerializer(read_only=True)
    comments = CommentSerializer(read_only=True, many=True)

    def update(self, instance, validated_data):
        instance.text = validated_data.get('text')
        instance.save()
        return instance

    class Meta:
        model = Answer
        fields = ('id', 'url', 'question', 'author', 'text', 'comments',
                  'vote_count', 'created_timestamp', 'modified_timestamp')
        # read_only_fields = ('author', 'created_timestamp', 'modified_timestamp')
