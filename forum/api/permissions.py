from rest_framework import exceptions


def author_or_forbidden(obj, user):
    if obj.author != user:
        raise exceptions.PermissionDenied('You are not the owner of this object')
