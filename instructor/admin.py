from django.contrib import admin

from instructor.models import Instructor

# Register your models here.
admin.site.register(Instructor)