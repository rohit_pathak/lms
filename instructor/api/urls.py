from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns

from instructor.api import views

urlpatterns = [
    url(r'^$', views.InstructorList.as_view(), name='instructor-list'),
    url(r'^(?P<pk>\d+)/$', views.InstructorDetail.as_view(), name='instructor-detail'),
    url(r'^(?P<pk>\d+)/documents/$', views.InstructorDocuments.as_view(), name='instructor-documents'),
    # url(r'^(?P<pk>\d+)/courses/$', views.InstructorCourses.as_view(), name='instructor-courses'),
]

urlpatterns = format_suffix_patterns(urlpatterns)
