from rest_framework import serializers

from academy.api.serializers import AcademyRef
from accounts.api.serializers import UserSerializer
from instructor.models import Instructor


class InstructorRef(serializers.HyperlinkedModelSerializer):
    first_name = serializers.StringRelatedField(source='user.first_name', read_only=True)
    last_name = serializers.StringRelatedField(source='user.last_name', read_only=True)
    email = serializers.StringRelatedField(source='user.email', read_only=True)

    class Meta:
        model = Instructor
        fields = ('url', 'user', 'first_name', 'last_name', 'email', 'academy', 'identification', 'is_active')


class InstructorDetailSerializer(serializers.HyperlinkedModelSerializer):
    user = UserSerializer(read_only=True)
    academy = AcademyRef(read_only=True)

    class Meta:
        model = Instructor
        fields = ('url', 'user', 'academy', 'identification', 'notes', 'date_joined', 'is_active')
        read_only_fields = ('date_joined',)
