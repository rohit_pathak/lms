from rest_framework import exceptions


def user_is_instructor_or_forbidden(user, instructor):
    if instructor.user != user:
        raise exceptions.PermissionDenied(detail='You are not this instructor')
