from django.shortcuts import get_object_or_404
from rest_framework import generics
from rest_framework.parsers import MultiPartParser
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

import utils
from academy.api.permissions import IsAdminInAcademy, IsAdminInAcademyOfObject
from document.api.serializers import DocumentSerializer
from document.models import create_document
from instructor.api.permissions import user_is_instructor_or_forbidden
from instructor.api.serializers import InstructorRef, InstructorDetailSerializer
from instructor.models import Instructor


class InstructorList(generics.CreateAPIView):
    """
    Add an instructor to an academy (only allowed for academy admins).
    """
    queryset = Instructor.objects.all()
    serializer_class = InstructorRef
    permission_classes = (IsAuthenticated, IsAdminInAcademy,)

    def perform_create(self, serializer):
        self.check_object_permissions(self.request, serializer.validated_data.get('academy'))
        serializer.save()


class InstructorDetail(generics.RetrieveUpdateAPIView):
    """
    Get or update an instructor's details (only allowed for academy admins).
    """
    queryset = Instructor.objects.all()
    serializer_class = InstructorDetailSerializer
    permission_classes = (IsAuthenticated, IsAdminInAcademyOfObject,)


class InstructorDocuments(APIView):
    """
    Upload a file for an instructor or retrieve all files uploaded by an instructor.
    """
    permission_classes = (IsAuthenticated,)
    parser_classes = (MultiPartParser,)

    def get(self, request, *args, **kwargs):
        instructor = get_object_or_404(Instructor, pk=self.kwargs.get('pk'))
        user_is_instructor_or_forbidden(self.request.user, instructor)
        serializer = DocumentSerializer(instructor.documents.all(), many=True, context={'request': request})
        return Response(serializer.data)

    def post(self, request, *args, **kwargs):
        instructor = get_object_or_404(Instructor, pk=self.kwargs.get('pk'))
        user_is_instructor_or_forbidden(self.request.user, instructor)
        file_object = request.data['file']
        # append number to name if file with this name already exists
        file_key = utils.generate_document_key_name(file_object.name, instructor.academy.short_name,
                                                    instructor_id=instructor.id)
        document = create_document(file_object, file_key, request.user)
        instructor.documents.add(document)
        instructor.save()
        return Response(data=DocumentSerializer(document, context={'request': request}).data, status=204)
