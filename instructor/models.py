from __future__ import unicode_literals

from django.conf import settings
from django.db import models

from academy.models import Academy
from document.models import Document


class Instructor(models.Model):
    academy = models.ForeignKey(Academy)
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    identification = models.CharField(max_length=20, null=True, blank=True)
    notes = models.TextField(null=True, blank=True)
    date_joined = models.DateTimeField(auto_now_add=True, blank=True)
    is_active = models.BooleanField(default=True)
    documents = models.ManyToManyField(Document)

    def get_name(self, ):
        return self.user.get_full_name()

    def __unicode__(self, ):
        return '%s | %s (%s)' % (self.user.email, self.academy.name, self.identification)

    class Meta:
        unique_together = (('academy', 'user'),)
