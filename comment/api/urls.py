from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns

from comment.api import views

urlpatterns = [
    url(r'^(?P<pk>\d+)/$', views.CommentDetail.as_view(), name='comment-detail'),
]

urlpatterns = format_suffix_patterns(urlpatterns)
