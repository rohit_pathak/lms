from rest_framework import serializers

from comment.models import Comment


class CommentSerializer(serializers.HyperlinkedModelSerializer):
    author_name = serializers.StringRelatedField(source='author.get_full_name', read_only=True)

    class Meta:
        model = Comment
        fields = ('url', 'author_name', 'content', 'created_timestamp', 'modified_timestamp')
