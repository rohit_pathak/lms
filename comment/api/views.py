from rest_framework import generics
from rest_framework.permissions import IsAuthenticated

from comment.api.serializers import CommentSerializer
from comment.models import Comment
from forum.api.permissions import author_or_forbidden


class CommentDetail(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = CommentSerializer
    permission_classes = (IsAuthenticated,)
    queryset = Comment.objects.all()

    def get(self, request, *args, **kwargs):
        author_or_forbidden(self.get_object(), self.request.user)
        return self.retrieve(request, *args, **kwargs)

    def perform_update(self, serializer):
        author_or_forbidden(self.get_object(), self.request.user)
        serializer.save()

    def perform_destroy(self, instance):
        author_or_forbidden(self.get_object(), self.request.user)
        instance.delete()
