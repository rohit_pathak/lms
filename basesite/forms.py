import textwrap

from django import forms
from django.conf import settings
from django.core.mail import send_mail


class BugReportForm(forms.Form):
    title = forms.CharField(max_length=150)
    description = forms.CharField(max_length=1000)

    def save(self, **kwargs):
        subject = "[Bug Report] %s" % (self.cleaned_data['title'],)
        message = "%s submitted the following bug report \n\n %s" % (kwargs.get('user').get_full_name(),
                                                                     self.cleaned_data.get('description'))
        send_mail(subject=subject, message=message, from_email=settings.DEFAULT_FROM_EMAIL,
                  recipient_list=['rohit.pathak@coursetrack.org', 'shreyans.jain@coursetrack.org'], fail_silently=True)


class DemoRequestForm(forms.Form):
    name = forms.CharField(max_length=150)
    email = forms.EmailField(max_length=150)
    phone = forms.CharField(max_length=20)
    comments = forms.CharField(max_length=200, required=False)

    def save(self, **kwargs):
        d = self.cleaned_data
        subject = "[Demo Request] %s" % (d['name'],)
        message = """
        Name: %s
        Email: %s
        Phone: %s
        Additional Information: %s
        """ % (d['name'], d['email'], d['phone'], d['comments'])
        send_mail(subject=subject, message=textwrap.dedent(message), from_email=settings.DEFAULT_FROM_EMAIL,
                  recipient_list=['rohit.pathak@coursetrack.org',
                                  'rohitpathak.3@gmail.com', 'shreyans.jain@coursetrack.org'], fail_silently=True)
