from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import render

from basesite.forms import BugReportForm, DemoRequestForm


def index(request):
    return HttpResponseRedirect(reverse('login-page'))


@login_required
def bug_report(request):
    if request.method == 'GET':
        return render(request, 'bug-report.html')
    elif request.method == 'POST':
        form = BugReportForm(request.POST)
        if form.is_valid():
            form.save(user=request.user)
        return render(request, 'bug-report-success.html')


def about_page(request):
    if request.method == 'POST':
        form = DemoRequestForm(request.POST)
        if form.is_valid():
            form.save()
        return render(request, 'demo-confirmation.html')
    return render(request, 'about.html')
