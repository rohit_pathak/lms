from rest_framework import serializers

from document.models import Document


class DocumentSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Document
        fields = ('id', 'url', 'owner', 'name', 'content_key', 'created_timestamp', 'modified_timestamp')
        read_only_fields = ('created_timestamp', 'modified_timestamp')
