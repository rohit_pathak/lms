from rest_framework import generics
from rest_framework.permissions import IsAuthenticated

from document.api.permissions import IsDocumentOwner
from document.api.serializers import DocumentSerializer
from document.models import Document


class DocumentDetail(generics.RetrieveAPIView):
    """
    Get a document
    """
    queryset = Document.objects.all()
    serializer_class = DocumentSerializer
    permission_classes = (IsAuthenticated, IsDocumentOwner)
