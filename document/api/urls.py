from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns

from document.api import views

urlpatterns = [
    url(r'^(?P<pk>\d+)/$', views.DocumentDetail.as_view(), name='document-detail'),
]

urlpatterns = format_suffix_patterns(urlpatterns)
