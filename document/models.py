from __future__ import unicode_literals

import logging
import traceback

import boto3 as boto
from django.conf import settings
from django.db import models
from django.dispatch import receiver

logger = logging.getLogger(__name__)


class Document(models.Model):
    owner = models.ForeignKey(settings.AUTH_USER_MODEL)
    name = models.CharField(max_length=75)
    content_key = models.CharField(max_length=200, unique=True)
    created_timestamp = models.DateTimeField(auto_now_add=True)
    modified_timestamp = models.DateTimeField(auto_now=True)

    def generate_download_url(self, ):
        s3 = boto.client('s3')
        url = s3.generate_presigned_url(
            ClientMethod='get_object',
            Params={
                'Bucket': settings.S3_BUCKET_NAME,
                'Key': self.content_key
            }
        )
        return url

    def __unicode__(self):
        return self.name


def create_document(file_object, key_name, owner):
    try:
        s3 = boto.resource('s3')
        s3.Object(settings.S3_BUCKET_NAME, key_name).put(Body=file_object)
    except Exception, err:
        logger.error("Could not create object in s3")
        logger.error(traceback.print_exc())
        return
    document = Document.objects.create(owner=owner, name=file_object.name, content_key=key_name)
    return document


@receiver(models.signals.post_delete, sender=Document)
def delete_document_callback(sender, instance, **kwargs):
    s3 = boto.client('s3')
    s3.delete_object(Bucket=settings.S3_BUCKET_NAME, Key=instance.content_key)
