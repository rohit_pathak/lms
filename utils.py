import binascii
import os


def generate_document_key_name(file_name, academy_short_name, instructor_id=None, course_id=None):
    assert file_name is not None
    assert academy_short_name is not None
    assert instructor_id is not None or course_id is not None
    res = academy_short_name + '/'
    if course_id is not None:
        res += 'course' + str(course_id) + '/'
    elif instructor_id is not None:
        res += 'instructor' + str(instructor_id) + '/'
    res += binascii.b2a_hex(os.urandom(8)) + '-' + file_name
    return res


def send_email(subject='', plain_body='', html_body='', to=None, bcc=None, cc=None):
    pass
