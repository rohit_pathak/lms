from __future__ import unicode_literals

from django.db import models

from announcement.models import Announcement
from course.models import Course
from courseresource.models import TopicItem


class Notification(models.Model):
    INSTRUCTORS = 'INSTRUCTORS'
    STUDENTS = 'STUDENTS'
    NOTIFY_TO_CHOICES = (
        (INSTRUCTORS, 'Instructors'),
        (STUDENTS, 'Students'),
    )
    ANNOUNCEMENT = 'ANNOUNCEMENT'
    RESOURCE_ITEM = 'RESOURCE'
    FORUM = 'FORUM'
    NOTIFICATION_CATEGORY_CHOICES = (
        (ANNOUNCEMENT, 'Announcement'),
        (RESOURCE_ITEM, 'Resource Item'),
        (FORUM, 'Forum'),
    )
    course = models.ForeignKey(Course)
    category = models.CharField(max_length=25, choices=NOTIFICATION_CATEGORY_CHOICES)
    message = models.CharField(max_length=220)
    notify_to = models.CharField(max_length=25, choices=NOTIFY_TO_CHOICES, default=STUDENTS)
    linked_announcement = models.ForeignKey(Announcement, blank=True, null=True)
    linked_resource = models.ForeignKey(TopicItem, blank=True, null=True)
    created_timestamp = models.DateTimeField(auto_now_add=True, blank=True)


