from notification.models import Notification


def notify(course, category, message, notify_to=Notification.STUDENTS, linked_announcement=None, linked_resource=None):
    return Notification.objects.create(course=course, category=category, message=message, notify_to=notify_to,
                                       linked_announcement=linked_announcement, linked_resource=linked_resource)
