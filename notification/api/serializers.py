from django.core.urlresolvers import reverse
from rest_framework import serializers

from notification.models import Notification


class NotificationSerializer(serializers.ModelSerializer):
    link = serializers.SerializerMethodField(read_only=True)

    def get_link(self, obj):
        if obj.category == Notification.ANNOUNCEMENT and obj.linked_announcement is not None:
            return reverse('course-announcement-detail-page',
                           args=(obj.linked_announcement.course.pk, obj.linked_announcement.pk))
        elif obj.category == Notification.RESOURCE_ITEM and obj.linked_resource is not None:
            return reverse('course-resource-item-detail-page',
                           args=(obj.linked_resource.topic.course.pk, obj.linked_resource.pk))

    class Meta:
        model = Notification
        fields = ('id', 'course', 'category', 'message', 'notify_to', 'link', 'created_timestamp')
