from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect


def index(request):
    # return render(request, 'index.html')
    return HttpResponseRedirect(reverse('login-page'))
