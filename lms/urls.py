"""lms URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""

from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.auth import views as django_auth_views

import accounts.views
import basesite.views
from academy.api import urls as academy_api_urls
from accounts.api import urls as accounts_api_urls
from announcement.api import urls as announcement_api_urls
from comment.api import urls as comment_api_urls
from course.api import urls as course_api_urls
from course_module.api import urls as course_module_api_urls
from gradebook.api import urls as gradebook_api_urls
from instructor.api import urls as instructor_api_urls
from quiz.api import urls as quiz_api_urls
from student.api import urls as student_api_urls

urlpatterns = [
    url(r'^admin/', admin.site.urls),

    # API
    url(r'^api/users/', include(accounts_api_urls)),
    url(r'^api/academies/', include(academy_api_urls)),
    url(r'^api/instructors/', include(instructor_api_urls)),
    url(r'^api/students/', include(student_api_urls)),
    url(r'^api/courses/', include(course_api_urls)),
    url(r'^api/course-modules/', include(course_module_api_urls)),
    url(r'^api/courseresources/', include('courseresource.api.urls')),
    url(r'^api/documents/', include('document.api.urls')),
    url(r'^api/forum/', include('forum.api.urls')),
    url(r'^api/gradebook/', include(gradebook_api_urls)),
    url(r'^api/announcements/', include(announcement_api_urls)),
    url(r'^api/comments/', include(comment_api_urls)),
    url(r'^api/quizzes/', include(quiz_api_urls)),

    # API Docs
    url(r'^api-docs/', include('rest_framework_swagger.urls')),

    # Web app
    url(r'^$', basesite.views.index, name='home'),
    url(r'^about/$', basesite.views.about_page, name='about-page'),
    url(r'^bug-report/$', basesite.views.bug_report, name='bug-report-page'),
    url(r'^accounts/', include('registration.backends.default.urls')),
    url(r'^login/$', accounts.views.login_request, name='login-page'),
    url(r'^logout/$', accounts.views.logout_request, name='logout-page'),
    url(r'^dashboard/$', accounts.views.dashboard, name='dashboard-page'),
    url(r'^reset-password/$', django_auth_views.password_reset, name='password_reset'),
    url(r'^reset-password-done/$', django_auth_views.password_reset_done, name='password_reset_done'),
    url(r'^reset-password-complete/$', django_auth_views.password_reset_complete, name='password_reset_complete'),
    url(r'^reset-password-confirm/(?P<uidb64>[0-9A-Za-z]+)-(?P<token>.+)/$', django_auth_views.password_reset_confirm,
        name='password_reset_confirm'),
    url(r'^academy/(?P<short_name>[a-z0-9]+)/', include('academy.urls')),
    url(r'^courses/', include('course.urls')),
]

urlpatterns += [
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
]
