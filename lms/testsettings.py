from .settings import *

DEBUG = False
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': 'coursetrack_development',
    }
}

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
