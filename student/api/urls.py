from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns

from student.api import views

urlpatterns = [
    url(r'^$', views.StudentList.as_view(), name='student-list'),
    url(r'^(?P<pk>\d+)/$', views.StudentDetail.as_view(), name='student-detail'),
    # url(r'^(?P<pk>\d+)/courses/$', views.StudentCourses.as_view(), name='student-courses'),
]

urlpatterns = format_suffix_patterns(urlpatterns)
