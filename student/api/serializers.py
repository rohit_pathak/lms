from rest_framework import serializers

from academy.api.serializers import AcademyRef
from accounts.api.serializers import UserSerializer
from student.models import Student


class StudentRef(serializers.HyperlinkedModelSerializer):
    first_name = serializers.StringRelatedField(source='user.first_name')
    last_name = serializers.StringRelatedField(source='user.last_name')
    email = serializers.StringRelatedField(source='user.email')

    class Meta:
        model = Student
        fields = ('url', 'user', 'first_name', 'last_name', 'email', 'academy', 'identification', 'is_active')
        read_only_fields = ('first_name', 'last_name', 'email', 'is_active')


class StudentDetailSerializer(serializers.HyperlinkedModelSerializer):
    user = UserSerializer(read_only=True)
    academy = AcademyRef(read_only=True)

    class Meta:
        model = Student
        fields = ('url', 'user', 'academy', 'identification', 'notes', 'date_joined', 'is_active')
        read_only_fields = ('date_joined',)
