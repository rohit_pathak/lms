from rest_framework import generics
from rest_framework.permissions import IsAuthenticated

from academy.api.permissions import IsAdminInAcademy, IsAdminInAcademyOfObject
from student.api.serializers import StudentRef, StudentDetailSerializer
from student.models import Student


class StudentList(generics.CreateAPIView):
    """
    Add a student to an academy (only allowed for academy admins).
    """
    queryset = Student.objects.all()
    serializer_class = StudentRef
    permission_classes = (IsAuthenticated, IsAdminInAcademy,)

    def perform_create(self, serializer):
        """
        Create a student (can only be created if the user is an admin in the
        academy the student is to be created in).
        Overrides method in super class.
        :param serializer: the student serializer
        :return: None
        """
        self.check_object_permissions(self.request, serializer.validated_data.get('academy'))
        serializer.save()


class StudentDetail(generics.RetrieveUpdateAPIView):
    """
    Get or update a student's details (only allowed for academy admins).
    """
    queryset = Student.objects.all()
    serializer_class = StudentDetailSerializer
    permission_classes = (IsAuthenticated, IsAdminInAcademyOfObject,)
