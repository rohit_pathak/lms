from django.core.urlresolvers import reverse
from rest_framework import serializers

from timeline.models import StudentEvent, ANNOUNCEMENT, RESOURCE, FORUM


class StudentEventSerializer(serializers.ModelSerializer):
    link = serializers.SerializerMethodField(read_only=True)

    def get_link(self, obj):
        if obj.category == ANNOUNCEMENT and obj.announcement is not None:
            return reverse('course-announcement-detail-page', args=(obj.announcement.course.pk, obj.announcement.pk))
        elif obj.category == RESOURCE and obj.resource is not None:
            return reverse('course-resource-item-detail-page', args=(obj.resource.topic.course.pk, obj.resource.pk))
        elif obj.category == FORUM and obj.question is not None:
            return reverse('course-forum-question-detail-page', args=(obj.question.course.pk, obj.question.pk))

    class Meta:
        model = StudentEvent
        fields = ('id', 'course', 'category', 'message', 'link', 'created_timestamp')
