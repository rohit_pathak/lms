from __future__ import unicode_literals

from django.db import models
from django.dispatch import receiver

from announcement.models import Announcement
from course.models import Course
from courseresource.models import TopicItem, NoteItem, VideoItem, LinkItem
from forum.models import Question

ANNOUNCEMENT = 'ANNOUNCEMENT'
RESOURCE = 'RESOURCE'
FORUM = 'FORUM'


class StudentEvent(models.Model):
    STUDENT_NOTIFICATION_CATEGORY_CHOICES = (
        (ANNOUNCEMENT, 'Announcement'),
        (RESOURCE, 'Resource'),
        (FORUM, 'Forum'),
    )
    course = models.ForeignKey(Course)
    category = models.CharField(max_length=25, choices=STUDENT_NOTIFICATION_CATEGORY_CHOICES)
    message = models.CharField(max_length=220)
    announcement = models.ForeignKey(Announcement, blank=True, null=True)
    resource = models.ForeignKey(TopicItem, blank=True, null=True)
    question = models.ForeignKey(Question, blank=True, null=True)
    created_timestamp = models.DateTimeField(auto_now_add=True, blank=True)


class InstructorEvent(models.Model):
    INSTRUCTOR_NOTIFICATION_CATEGORY_CHOICES = (
        (FORUM, 'Forum'),
    )
    course = models.ForeignKey(Course)
    category = models.CharField(max_length=25, choices=INSTRUCTOR_NOTIFICATION_CATEGORY_CHOICES)
    message = models.CharField(max_length=220)
    question = models.ForeignKey(Question, blank=True, null=True)
    created_timestamp = models.DateTimeField(auto_now_add=True, blank=True)


@receiver(models.signals.post_save, sender=VideoItem)
@receiver(models.signals.post_save, sender=LinkItem)
@receiver(models.signals.post_save, sender=NoteItem)
def resource_created(sender, instance, created, **kwargs):
    if not created:
        return
    StudentEvent.objects.create(course=instance.topic.course, category=RESOURCE,
                                message='"%s" was added under resource "%s"' % (instance.name, instance.topic.name),
                                resource=instance)


@receiver(models.signals.post_save, sender=Announcement)
def announcement_created(sender, instance, created, **kwargs):
    if not created:
        return
    StudentEvent.objects.create(course=instance.course, category=ANNOUNCEMENT, message=instance.title,
                                announcement=instance)


@receiver(models.signals.post_save, sender=Question)
def question_created(sender, instance, created, **kwargs):
    if not created:
        return
    StudentEvent.objects.create(course=instance.course, category=FORUM, message=instance.title,
                                question=instance)
