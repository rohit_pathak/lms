from __future__ import unicode_literals

from django.conf import settings
from django.db import models
from django.dispatch import receiver
from registration.signals import user_activated

import constants
from academy.models import Academy
from student.models import Student


class Invitation(models.Model):
    """
    An invitation for someone to join an academy.
    Usually created by academy admins when adding students.
    """
    ROLE_CHOICES = (
        (constants.INSTRUCTOR, 'Instructor'),
        (constants.STUDENT, 'Student'),
        (constants.ADMIN, 'Admin')
    )
    invited_by = models.ForeignKey(settings.AUTH_USER_MODEL)
    academy = models.ForeignKey(Academy)
    email = models.EmailField()
    role = models.CharField(max_length=20, choices=ROLE_CHOICES)
    is_active = models.BooleanField(default=True)
    created_timestamp = models.DateTimeField(auto_now_add=True)
    modified_timestamp = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return "%s | %s" % (self.email, self.academy)

    class Meta:
        unique_together = ('email', 'academy')


@receiver(user_activated)
def put_in_academies(sender, **kwargs):
    print 'user activated!', kwargs['user']
    invitations = Invitation.objects.filter(email=kwargs['user'].email, is_active=True)
    for invitation in invitations:
        if invitation.role == constants.STUDENT:
            student = Student(user=kwargs['user'], academy=invitation.academy,
                              notes='Invited by %s' % (invitation.invited_by.get_full_name()))
            student.save()
        elif invitation.role == constants.INSTRUCTOR:
            pass
        invitation.is_active = False
        invitation.save()
