from django.contrib import admin

from gradebook.models import Topic, Grade

# Register your models here.
admin.site.register(Topic)
admin.site.register(Grade)
