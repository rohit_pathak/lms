from __future__ import unicode_literals

from django.db import models

from course.models import Course
from student.models import Student


class Topic(models.Model):
    """
    A topic is some assignment/quiz/test/exam that is gradable (e.g. Unit test 1 is a topic)
    """
    ASSIGNMENT = 'ASSIGNMENT'
    TEST = 'TEST'
    QUIZ = 'QUIZ'
    OTHER = 'OTHER'
    GRADE_CATEGORY_CHOICES = (
        (ASSIGNMENT, 'Assignment'),
        (QUIZ, 'Quiz'),
        (TEST, 'Test'),
        (OTHER, 'Other'),
    )
    course = models.ForeignKey(Course)
    name = models.CharField(max_length=100)
    category = models.CharField(max_length=20, choices=GRADE_CATEGORY_CHOICES, default=OTHER)
    maximum_marks = models.IntegerField(default=100)
    create_date = models.DateTimeField(auto_now_add=True, blank=True)

    def __unicode__(self, ):
        return '%s | %s' % (self.name, self.course)

    class Meta:
        unique_together = ('course', 'name')


class Grade(models.Model):
    """
    A Grade is the individual grade a student gets on a Topic
    """
    LETTER_GRADE_CHOICES = (
        ('A+', 'A+'),
        ('A', 'A'),
        ('B+', 'B+'),
        ('B', 'B'),
        ('C+', 'C+'),
        ('C', 'C'),
        ('D+', 'D+'),
        ('D', 'D'),
        ('F', 'F')
    )
    topic = models.ForeignKey(Topic)
    student = models.ForeignKey(Student)
    marks = models.IntegerField()
    letter_grade = models.CharField(max_length=2, choices=LETTER_GRADE_CHOICES, null=True, blank=True)
    comments = models.CharField(max_length=200, null=True, blank=True)

    @property
    def course(self):
        return self.topic.course

    def __unicode__(self, ):
        return '%s | %s - %s' % (self.student, self.topic, self.marks)

    class Meta:
        unique_together = ('topic', 'student')
