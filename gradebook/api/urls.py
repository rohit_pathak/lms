from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns

from gradebook.api import views

urlpatterns = [
    url(r'^topics/$', views.TopicList.as_view(), name='topic-list'),
    url(r'^topics/(?P<pk>\d+)/$', views.TopicDetail.as_view(), name='topic-detail'),
    url(r'^topics/(?P<pk>\d+)/grades/$', views.TopicGrades.as_view(), name='topic-grades'),
    url(r'^grades/$', views.GradeList.as_view(), name='grade-list'),
    url(r'^grades/(?P<pk>\d+)/$', views.GradeDetail.as_view(), name='grade-detail'),
]

urlpatterns = format_suffix_patterns(urlpatterns)
