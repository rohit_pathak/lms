from django.shortcuts import get_object_or_404
from rest_framework import generics
from rest_framework.exceptions import PermissionDenied
from rest_framework.permissions import IsAuthenticated

from course.api.permissions import IsCourseInstructorOfResourceOrStudentReadOnly, course_instructor_or_forbidden
from gradebook.api.serializers import TopicSerializer, GradeSerializer
from gradebook.models import Topic, Grade


class TopicList(generics.CreateAPIView):
    queryset = Topic.objects.all()
    serializer_class = TopicSerializer
    permission_classes = (IsAuthenticated,)

    def perform_create(self, serializer):
        course_instructor_or_forbidden(self.request.user, serializer.validated_data.get('course'))
        serializer.save()


class TopicDetail(generics.RetrieveUpdateAPIView):
    queryset = Topic.objects.all()
    serializer_class = TopicSerializer
    permission_classes = (IsAuthenticated, IsCourseInstructorOfResourceOrStudentReadOnly)


class GradeList(generics.CreateAPIView):
    queryset = Grade.objects.all()
    serializer_class = GradeSerializer
    permission_classes = (IsAuthenticated,)

    def perform_create(self, serializer):
        course = serializer.validated_data.get('topic').course
        course_instructor_or_forbidden(self.request.user, course)
        if not serializer.validated_data.get('student') in course.students.all():
            raise PermissionDenied()
        serializer.save()


class GradeDetail(generics.RetrieveUpdateAPIView):
    queryset = Grade.objects.all()
    serializer_class = GradeSerializer
    permission_classes = (IsAuthenticated,)

    def perform_update(self, serializer):
        course_instructor_or_forbidden(self.request.user, serializer.validated_data.get('topic').course)
        serializer.save()

    def retrieve(self, request, *args, **kwargs):
        if self.get_object().student.user != self.request.user:
            raise PermissionDenied(detail='This is not your grade.')
        super(GradeDetail, self).retrieve(request, *args, **kwargs)


class TopicGrades(generics.ListAPIView):
    """
    Returns all grades for a topic.
    The user retrieving must be an instructor of the topic's course.
    """
    serializer_class = GradeSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        topic = get_object_or_404(Topic, pk=self.kwargs['pk'])
        course_instructor_or_forbidden(self.request.user, topic.course)
        return topic.grade_set.all()
