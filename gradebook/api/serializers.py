from rest_framework import serializers

from gradebook.models import Grade, Topic


class TopicSerializer(serializers.HyperlinkedModelSerializer):
    def update(self, instance, validated_data):
        # ignore updating course because that should not be changed
        instance.name = validated_data.get('name', instance.name)
        instance.category = validated_data.get('category', instance.category)
        instance.maximum_marks = validated_data.get('maximum_marks', instance.maximum_marks)
        instance.save()
        return instance

    class Meta:
        model = Topic
        fields = ('url', 'id', 'course', 'name', 'category', 'maximum_marks', 'create_date')
        read_only_fields = ('create_date',)


class GradeSerializer(serializers.HyperlinkedModelSerializer):
    def update(self, instance, validated_data):
        instance.marks = validated_data.get('marks', instance.marks)
        instance.letter_grade = validated_data.get('letter_grade', instance.letter_grade)
        instance.comments = validated_data.get('comments', instance.comments)
        instance.save()
        return instance

    class Meta:
        model = Grade
        fields = ('url', 'id', 'topic', 'student', 'marks', 'letter_grade', 'comments')
        required_fields = ('topic', 'student', 'marks')
