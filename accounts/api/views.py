from django.contrib.auth import get_user_model
from django.shortcuts import get_object_or_404
from rest_framework import generics
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.views import APIView

from accounts.api.permissions import IsOwnUser
from accounts.api.serializers import UserSerializer
from course.api.permissions import course_student_or_forbidden
from course.api.serializers import CourseRef
from course.models import Course
from student.api.serializers import StudentRef

User = get_user_model()


class UserList(generics.ListAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (IsAuthenticated, IsOwnUser)


class UserDetail(generics.RetrieveUpdateAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (IsAuthenticated, IsOwnUser)


class LoggedInUserDetails(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, format=None):
        return Response(UserSerializer(request.user, context={'request': request}).data)


class CourseStudentProfile(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, course_id, format=None):
        course = get_object_or_404(Course, pk=course_id)
        course_student_or_forbidden(self.request.user, course)
        student_profile = course.students.get(user=self.request.user)
        return Response(StudentRef(student_profile, context={'request': request}).data)


class CurrentUserCourses(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, format=None):
        response = {}
        response['student'] = CourseRef(request.user.get_courses_as_student(), many=True,
                                        context={'request': request}).data
        response['instructor'] = CourseRef(request.user.get_courses_as_instructor(), many=True,
                                           context={'request': request}).data
        return Response(response)


class UserByEmail(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request, format=None):
        if User.objects.filter(email=request.data.get('email')).exists():
            return Response(reverse('customuser-detail', args=(User.objects.get(email=request.data.get('email')).pk,),
                                    request=request))
        else:
            return Response('')
