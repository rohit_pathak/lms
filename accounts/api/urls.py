from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns

from accounts.api import views

urlpatterns = [
    url(r'^$', views.UserList.as_view(), name='customuser-list'),
    url(r'^(?P<pk>\d+)/$', views.UserDetail.as_view(), name='customuser-detail'),
    url(r'^current/$', views.LoggedInUserDetails.as_view(), name='current-user-detail'),
    url(r'^current/(?P<course_id>\d+)/studentprofile/$', views.CourseStudentProfile.as_view(),
        name='current-user-course-student-profile'),
    url(r'^current/courses/$', views.CurrentUserCourses.as_view(), name='current-user-courses'),

    url(r'^queryemail/$', views.UserByEmail.as_view(), name='user-by-email'),
]

urlpatterns = format_suffix_patterns(urlpatterns)
