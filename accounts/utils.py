import hashlib
import random

from django.core.mail import send_mail


def send_activation_mail(user, uri):
    salt = hashlib.sha1(str(random.random())).hexdigest()[:5]
    email = user.email
    if isinstance(email, unicode):
        email = email.encode('utf-8')

    user.activation_key = hashlib.sha1(salt + email).hexdigest()
    user.save()
    subject = 'Activate your coursetrack account'
    body = 'Click on the following link to activate you account: %s%s' % (uri, profile.activation_key)
    from_sender = 'rohitpathak.3@gmail.com'
    send_mail(subject, body, from_sender, [email], fail_silently=False)
