from django.contrib.auth import authenticate, login, logout, get_user_model
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.shortcuts import render

from .forms import LoginForm

User = get_user_model()


def login_request(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect('/dashboard/')

    if request.method == 'POST':  # user is trying to log in
        next_url = request.GET.get('next')
        form = LoginForm(request.POST)
        if form.is_valid():
            email = form.cleaned_data['email']  # our username is the email
            password = form.cleaned_data['password']
            user = authenticate(username=email, password=password)
            login(request, user)
            if next_url is not None:
                return HttpResponseRedirect(next_url)
            else:
                return HttpResponseRedirect('/dashboard/')
        else:  # form is not valid
            return render(request, 'accounts/login.html', {'form': form})

    elif request.method == 'GET':
        next_url = request.GET.get('next')
        return render(request, 'accounts/login.html', {'next_url': next_url})

    else:
        return HttpResponseRedirect('/')


def logout_request(request):
    if request.user.is_authenticated():
        logout(request)
        return render(request, 'accounts/logout-success.html')
    else:
        return HttpResponseRedirect('/dashboard/')


@login_required
def dashboard(request):
    u = request.user
    academies = set()
    for p in u.student_set.filter(is_active=True):
        academies.add(p.academy)
    for p in u.instructor_set.filter(is_active=True):
        academies.add(p.academy)
    for a in u.academy_set.all():
        academies.add(a)
    context = {'academies': academies}
    return render(request, 'accounts/dashboard.html', context)
