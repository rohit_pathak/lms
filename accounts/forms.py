from django import forms
from django.contrib.auth import authenticate
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from registration.forms import RegistrationFormUniqueEmail
from .models import CustomUser


class CustomUserCreationForm(UserCreationForm):

    """
    A form that creates a user, with no privileges, from the given email and
    password.
    """

    def __init__(self, *args, **kargs):
        super(CustomUserCreationForm, self).__init__(*args, **kargs)

    class Meta:
        model = CustomUser
        fields = ("email",)


class CustomUserChangeForm(UserChangeForm):

    """A form for updating users. Includes all the fields on
    the user, but replaces the password field with admin's
    password hash display field.
    """

    class Meta:
        model = CustomUser
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(CustomUserChangeForm, self).__init__(*args, **kwargs)
        f = self.fields.get('user_permissions', None)
        if f is not None:
            f.queryset = f.queryset.select_related('content_type')


class LoginForm(forms.Form):
    email = forms.EmailField(label=u'Email', max_length=150)
    password = forms.CharField(
        label=u'Password', max_length=100, widget=forms.PasswordInput(render_value=False))

    def clean(self):
        email = self.cleaned_data.get('email')  # our username is the email
        password = self.cleaned_data.get('password')
        user = authenticate(username=email, password=password)

        if user is not None:
            return self.cleaned_data
        else:
            raise forms.ValidationError('Invalid username and password combination')


class CoursetrackRegistrationForm(RegistrationFormUniqueEmail):
    first_name = forms.CharField(max_length=50)
    last_name = forms.CharField(max_length=50)

    def save(self, commit=True):
        user = super(CoursetrackRegistrationForm, self).save()
        user.first_name = self.cleaned_data.get('first_name')
        user.last_name = self.cleaned_data.get('last_name')
        user.save()
        return user
