import textwrap

from django.core.mail import EmailMessage
from django.core.urlresolvers import reverse
from django.shortcuts import get_object_or_404
from rest_framework import generics
from rest_framework import status
from rest_framework.exceptions import PermissionDenied
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

import constants
from course.api.permissions import course_instructor_or_forbidden, course_member_or_forbidden, \
    document_in_course_or_forbidden
from courseresource.models import ResourceTopic, TopicItem, NoteItem, VideoItem, LinkItem
from .serializers import ResourceTopicRef, ResourceTopicSerializer, NoteItemSerializer, VideoItemSerializer, \
    LinkItemSerializer, TopicItemRef


class ResourceTopicList(generics.CreateAPIView):
    queryset = ResourceTopic.objects.all()
    serializer_class = ResourceTopicRef
    permission_classes = (IsAuthenticated,)

    def perform_create(self, serializer):
        course_instructor_or_forbidden(self.request.user, serializer.validated_data.get('course'))
        serializer.save()


class ResourceTopicDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = ResourceTopic.objects.all()
    serializer_class = ResourceTopicSerializer
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        resource_topic = self.get_object()
        course_member_or_forbidden(self.request.user, resource_topic.course)
        return self.retrieve(request, *args, **kwargs)

    def perform_update(self, serializer):
        course_instructor_or_forbidden(self.request.user, serializer.validated_data.get('course'))
        serializer.save()

    def perform_destroy(self, instance):
        course_instructor_or_forbidden(self.request.user, instance.course)
        instance.delete()


class TopicItems(APIView):
    """
    All topic items (under all categories) for a topic.
    """
    permission_classes = (IsAuthenticated,)

    def get(self, request, pk, format=None):
        topic = get_object_or_404(ResourceTopic, pk=pk)
        course_member_or_forbidden(self.request.user, topic.course)
        serializer = TopicItemRef(topic.topicitem_set, many=True, context={'request': request})
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        topic = get_object_or_404(ResourceTopic, pk=pk)
        course_instructor_or_forbidden(request.user, topic.course)
        serializers = []
        for raw_topic_item in request.data:
            topic_item = get_object_or_404(TopicItem, topic=topic, pk=raw_topic_item['id'])
            serializer = TopicItemRef(topic_item, data=raw_topic_item, context={'request': request})
            if not serializer.is_valid():
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
            serializers.append(serializer)
        for serializer in serializers:
            serializer.save()
        return Response(status=status.HTTP_200_OK)


# Individual topic item views
class BaseTopicItemList(generics.CreateAPIView):
    permission_classes = (IsAuthenticated,)

    def perform_create(self, serializer):
        course_instructor_or_forbidden(self.request.user, serializer.validated_data.get('topic').course)
        item = serializer.save()
        email_subject = '%s: New resource added' % (item.topic.course.name,)
        email_body = """
        Hello,

        Your course %s at %s has a new item "%s" in resource "%s".
        Check out the course website for more details at %s.

        Thanks,
        The Coursetrack Team
        """ % (item.topic.course.name, item.topic.course.academy.name, item.name, item.topic.name,
               self.request.build_absolute_uri(reverse('course-home-page', args=(item.topic.course.pk,))))
        email = EmailMessage(subject=email_subject, body=textwrap.dedent(email_body),
                             to=[constants.UNDISCLOSED_RECIPIENTS],
                             bcc=[s.user.email for s in item.topic.course.students.all()])
        email.send(fail_silently=True)


class BaseTopicItemDetail(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        course_member_or_forbidden(self.request.user, self.get_object().topic.course)
        return self.retrieve(request, *args, **kwargs)

    def perform_update(self, serializer):
        course_instructor_or_forbidden(self.request.user, serializer.validated_data.get('topic').course)
        serializer.save()

    def perform_destroy(self, instance):
        course_instructor_or_forbidden(self.request.user, instance.topic.course)
        instance.delete()


class NoteItemList(BaseTopicItemList):
    queryset = NoteItem.objects.all()
    serializer_class = NoteItemSerializer

    def perform_create(self, serializer):
        for doc in serializer.validated_data.get('documents', []):
            document_in_course_or_forbidden(doc, serializer.validated_data.get('topic').course)
        super(NoteItemList, self).perform_create(serializer)


class NoteItemDetail(BaseTopicItemDetail):
    queryset = NoteItem.objects.all()
    serializer_class = NoteItemSerializer

    def perform_update(self, serializer):
        for doc in serializer.validated_data.get('documents', []):
            document_in_course_or_forbidden(doc, serializer.validated_data.get('topic').course)
        super(NoteItemDetail, self).perform_update(serializer)


class NoteItemDocumentDownload(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, pk, document_id, format=None):
        note = get_object_or_404(NoteItem, pk=pk)
        course_member_or_forbidden(request.user, note.topic.course)
        if not note.documents.filter(pk=document_id).exists():
            raise PermissionDenied(detail="You don't have permission to access this file.")
        return Response(note.documents.get(pk=document_id).generate_download_url())


class VideoItemList(BaseTopicItemList):
    queryset = VideoItem.objects.all()
    serializer_class = VideoItemSerializer


class VideoItemDetail(BaseTopicItemDetail):
    queryset = VideoItem.objects.all()
    serializer_class = VideoItemSerializer


class LinkItemList(BaseTopicItemList):
    queryset = LinkItem.objects.all()
    serializer_class = LinkItemSerializer


class LinkItemDetail(BaseTopicItemDetail):
    queryset = LinkItem.objects.all()
    serializer_class = LinkItemSerializer
