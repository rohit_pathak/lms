from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from rest_framework.reverse import reverse

from courseresource.models import ResourceTopic, TopicItem, VideoItem, NoteItem, LinkItem
from document.api.serializers import DocumentSerializer


class ResourceTopicRef(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = ResourceTopic
        fields = ('id', 'url', 'course', 'name', 'sort_order')


class TopicItemRef(serializers.HyperlinkedModelSerializer):
    url = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = TopicItem
        fields = ('id', 'url', 'topic', 'category', 'name', 'sort_order')
        read_only_fields = ('category',)

    def get_url(self, obj):
        request = self.context.get('request')
        if request is None:
            raise ValidationError(detail='Topic Item Ref cannot be serialized without the request context')

        if obj.category == TopicItem.NOTE:
            return reverse('noteitem-detail', args=(obj.pk,), request=request)
        elif obj.category == TopicItem.LINK:
            return reverse('linkitem-detail', args=(obj.pk,), request=request)
        elif obj.category == TopicItem.VIDEO:
            return reverse('videoitem-detail', args=(obj.pk,), request=request)
        elif obj.category == TopicItem.QUIZ:
            return reverse('quiz-detail', args=(obj.pk,), request=request)

        return None

    def update(self, instance, validated_data):
        instance.name = validated_data.get('name', instance.name)
        instance.sort_order = validated_data.get('sort_order', instance.sort_order)
        instance.save()
        return instance


class ResourceTopicSerializer(serializers.HyperlinkedModelSerializer):
    topicitem_set = TopicItemRef(many=True, read_only=True)

    class Meta:
        model = ResourceTopic
        fields = ('id', 'url', 'course', 'name', 'sort_order', 'topicitem_set')


class VideoItemSerializer(serializers.HyperlinkedModelSerializer):
    def save(self, **kwargs):
        return super(VideoItemSerializer, self).save(category=TopicItem.VIDEO, **kwargs)

    def validate(self, attrs):
        if 'youtube' not in attrs['video_url']:
            raise ValidationError('The linked video must be a YouTube video.')
        return attrs

    class Meta:
        model = VideoItem
        fields = ('id', 'url', 'topic', 'category', 'name', 'video_url', 'sort_order')
        read_only_fields = ('category',)


class NoteItemSerializer(serializers.HyperlinkedModelSerializer):
    document_details = DocumentSerializer(source='documents', many=True, read_only=True)

    def save(self, **kwargs):
        return super(NoteItemSerializer, self).save(category=TopicItem.NOTE, **kwargs)

    class Meta:
        model = NoteItem
        fields = ('id', 'url', 'topic', 'category', 'name', 'text', 'documents', 'document_details', 'sort_order')
        read_only_fields = ('category', 'document_details')


class LinkItemSerializer(serializers.HyperlinkedModelSerializer):
    def save(self, **kwargs):
        return super(LinkItemSerializer, self).save(category=TopicItem.LINK, **kwargs)

    class Meta:
        model = LinkItem
        fields = ('id', 'url', 'topic', 'category', 'name', 'link_url', 'sort_order')
        read_only_fields = ('category',)
