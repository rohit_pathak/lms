from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns

from courseresource.api import views

urlpatterns = [
    url(r'^$', views.ResourceTopicList.as_view(), name='resourcetopic-list'),
    url(r'^(?P<pk>\d+)/$', views.ResourceTopicDetail.as_view(), name='resourcetopic-detail'),
    url(r'^(?P<pk>\d+)/topicitems/$', views.TopicItems.as_view(), name='resourcetopic-items'),
    url(r'^noteitems/$', views.NoteItemList.as_view(), name='noteitem-list'),
    url(r'^noteitems/(?P<pk>\d+)/$', views.NoteItemDetail.as_view(), name='noteitem-detail'),
    url(r'^noteitems/(?P<pk>\d+)/documents/(?P<document_id>\d+)/download/$', views.NoteItemDocumentDownload.as_view(),
        name='noteitem-document-download'),
    url(r'^videoitems/$', views.VideoItemList.as_view(), name='videoitem-list'),
    url(r'^videoitems/(?P<pk>\d+)/$', views.VideoItemDetail.as_view(), name='videoitem-detail'),
    url(r'^linkitems/$', views.LinkItemList.as_view(), name='linkitem-list'),
    url(r'^linkitems/(?P<pk>\d+)/$', views.LinkItemDetail.as_view(), name='linkitem-detail'),
]

urlpatterns = format_suffix_patterns(urlpatterns)
