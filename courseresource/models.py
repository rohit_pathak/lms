from __future__ import unicode_literals

from django.db import models

from course.models import Course
from document.models import Document


class ResourceTopic(models.Model):
    course = models.ForeignKey(Course)
    name = models.CharField(max_length=100)
    sort_order = models.IntegerField(default=0)

    def __str__(self):
        return '{} | {}'.format(self.name, self.course)


class TopicItem(models.Model):
    VIDEO = 'video'
    NOTE = 'note'
    LINK = 'link'
    QUIZ = 'quiz'
    CATEGORY_CHOICES = (
        (VIDEO, 'Video'),
        (NOTE, 'Note'),
        (LINK, 'Link'),
        (QUIZ, 'Quiz')
    )
    topic = models.ForeignKey(ResourceTopic)
    category = models.CharField(max_length=50, choices=CATEGORY_CHOICES)
    name = models.CharField(max_length=100)
    sort_order = models.IntegerField(default=0)

    def __unicode__(self):
        return '{} ({}) | {}, {}'.format(self.name, self.category, self.topic.course, self.topic.name)


class VideoItem(TopicItem):
    video_url = models.URLField(max_length=200)

    @property
    def embed_url(self):
        if '/embed/' in self.video_url:
            return self.video_url
        else:
            return self.video_url.replace('watch?v=', 'embed/')


class NoteItem(TopicItem):
    text = models.TextField()
    documents = models.ManyToManyField(Document, blank=True)


class LinkItem(TopicItem):
    link_url = models.URLField(max_length=200)
