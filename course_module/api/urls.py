from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns

from course_module.api import views

urlpatterns = [
    url(r'^$', views.CourseModuleList.as_view(), name='coursemodule-list'),
    url(r'^(?P<pk>\d+)/$', views.CourseModuleDetail.as_view(), name='coursemodule-detail'),
]

urlpatterns = format_suffix_patterns(urlpatterns)
