from rest_framework import serializers

from course_module.models import CourseModule


class CourseModuleRef(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = CourseModule
        fields = ('url', 'course', 'name')


class CourseModuleDetailSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = CourseModule
        fields = ('url', 'course', 'name', 'content')
