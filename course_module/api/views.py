from rest_framework import generics
from rest_framework.exceptions import PermissionDenied
from rest_framework.permissions import IsAuthenticated

from course.api.permissions import IsCourseInstructorOfResourceOrStudentReadOnly
from course_module.api.serializers import CourseModuleRef, CourseModuleDetailSerializer
from course_module.models import CourseModule


class CourseModuleList(generics.CreateAPIView):
    queryset = CourseModule.objects.all()
    serializer_class = CourseModuleRef
    permission_classes = (IsAuthenticated,)

    def perform_create(self, serializer):
        if not serializer.validated_data.get('course').has_as_instructor(self.request.user):
            raise PermissionDenied(detail='You are not an instructor in this course.')
        serializer.save()


class CourseModuleDetail(generics.RetrieveUpdateAPIView):
    queryset = CourseModule.objects.all()
    serializer_class = CourseModuleDetailSerializer
    permission_classes = (IsAuthenticated, IsCourseInstructorOfResourceOrStudentReadOnly)
