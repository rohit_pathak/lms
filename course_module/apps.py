from __future__ import unicode_literals

from django.apps import AppConfig


class CourseModuleConfig(AppConfig):
    name = 'course_module'
