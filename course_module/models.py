from __future__ import unicode_literals

from django.db import models

from course.models import Course


class CourseModule(models.Model):
    course = models.ForeignKey(Course)
    name = models.CharField(max_length=100)
    content = models.TextField(null=True, blank=True)

    def __unicode__(self, ):
        return "%s | %s" % (self.name, self.course.name)
