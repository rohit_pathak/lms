from django.contrib import admin

from course_module.models import CourseModule

admin.site.register(CourseModule)
