"""
Important constants used across the project
"""

STUDENT = 'student'
INSTRUCTOR = 'instructor'
ADMIN = 'admin'

UNDISCLOSED_RECIPIENTS = 'Undisclosed Recipients <undisclosed@coursetrack.org>'
