from __future__ import unicode_literals

from django.conf import settings
from django.db import models

from courseresource.models import TopicItem
from student.models import Student


class Quiz(TopicItem):
    description = models.TextField(max_length=1000)
    maximum_attempts = models.IntegerField(default=1)

    @property
    def maximum_marks(self, ):
        return sum([x.maximum_marks for x in self.quizquestion_set.all()])


class AnswerChoice(models.Model):
    content = models.CharField(max_length=200)
    feedback = models.CharField(max_length=200, null=True, blank=True)

    def __str__(self):
        return self.content[:50]


class QuizQuestion(models.Model):
    QUESTION_TYPE_CHOICES = (
        ('SINGLE_RESPONSE', 'Single Response'),
        ('MULTI_RESPONSE', 'Multiple Response'),
    )
    quiz = models.ForeignKey(Quiz)
    content = models.TextField()
    maximum_marks = models.IntegerField()
    question_type = models.CharField(choices=QUESTION_TYPE_CHOICES, max_length=25)
    sort_order = models.IntegerField(default=0)
    answer_choices = models.ManyToManyField(AnswerChoice)
    correct_answers = models.ManyToManyField(AnswerChoice, related_name='answers_for_quiz_question', blank=True)
    correct_single_answer = models.ForeignKey(AnswerChoice, related_name='single_answer_for_quiz_question', null=True,
                                              blank=True)
    feedback = models.CharField(max_length=200, null=True, blank=True)

    # TODO: add created/modified timestamp fields
    # correct_answers is a field for questions that have multiple correct answers (future feature)

    def __str__(self):
        return "%s | %s (Course %s)" % (self.content[:40], self.quiz.name, self.quiz.topic.course.id)


class QuizResponse(models.Model):
    student_user = models.ForeignKey(settings.AUTH_USER_MODEL)
    quiz = models.ForeignKey(Quiz)
    attempt = models.IntegerField()
    created_timestamp = models.DateTimeField(auto_now_add=True)
    modified_timestamp = models.DateTimeField(auto_now=True)

    @property
    def score(self, ):
        return sum([question_response.score for question_response in self.quizquestionresponse_set.all()])

    @property
    def num_responses(self, ):
        return self.quizquestionresponse_set.all().count()


class QuizQuestionResponse(models.Model):
    quiz_question = models.ForeignKey(QuizQuestion)
    quiz_response = models.ForeignKey(QuizResponse)
    single_answer_response = models.ForeignKey(AnswerChoice)
    created_timestamp = models.DateTimeField(auto_now_add=True)
    modified_timestamp = models.DateTimeField(auto_now=True)

    @property
    def score(self, ):
        if self.quiz_question.question_type == 'SINGLE_RESPONSE':
            if self.single_answer_response == self.quiz_question.correct_single_answer:
                return self.quiz_question.maximum_marks
        return 0

    class Meta:
        unique_together = ('quiz_question', 'quiz_response')
