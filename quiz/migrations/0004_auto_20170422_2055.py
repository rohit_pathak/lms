# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2017-04-22 20:55
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('quiz', '0003_auto_20170422_2051'),
    ]

    operations = [
        migrations.AlterField(
            model_name='quizquestion',
            name='correct_answers',
            field=models.ManyToManyField(blank=True, related_name='answers_for_quiz_question', to='quiz.AnswerChoice'),
        ),
    ]
