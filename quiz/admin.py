# Register your models here.
from django.contrib import admin

from quiz.models import Quiz, AnswerChoice, QuizQuestion, QuizResponse, QuizQuestionResponse

admin.site.register(Quiz)
admin.site.register(AnswerChoice)
admin.site.register(QuizQuestion)
admin.site.register(QuizResponse)
admin.site.register(QuizQuestionResponse)
