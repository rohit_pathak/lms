from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns

from quiz.api import views

urlpatterns = [
    url(r'^$', views.QuizList.as_view(), name='quiz-list'),
    url(r'^(?P<pk>\d+)/$', views.InstructorQuizDetail.as_view(), name='quiz-detail'),
    url(r'^student-safe/(?P<pk>\d+)/$', views.StudentQuizDetail.as_view(), name='student-quiz-detail'),
    url(r'^(?P<pk>\d+)/bulk-update-questions/$', views.InstructorQuizQuestionBulkUpdate.as_view(),
        name='quizquestion-bulk-update'),
    url(r'^questions/$', views.InstructorQuizQuestionList.as_view(), name='quizquestion-list'),
    url(r'^questions/(?P<pk>\d+)/$', views.InstructorQuizQuestionDetail.as_view(), name='quizquestion-detail'),
    url(r'^answer-choices/$', views.AnswerChoiceList.as_view(), name='answerchoice-list'),
    url(r'^answer-choices/(?P<pk>\d+)/$', views.AnswerChoiceDetail.as_view(), name='answerchoice-detail'),
    url(r'^question-responses/(?P<pk>\d+)/$', views.QuizQuestionResponseDetail.as_view(),
        name='quizquestionresponse-detail'),
    url(r'^(?P<pk>\d+)/responses/$', views.QuizResponseList.as_view(), name='quiz-responses'),
    url(r'^quizresponses/(?P<pk>\d+)/$', views.QuizResponseDetail.as_view(), name='quizresponse-detail'),
]

urlpatterns = format_suffix_patterns(urlpatterns)
