from rest_framework import serializers
from rest_framework.exceptions import PermissionDenied

from courseresource.models import TopicItem
from quiz.models import Quiz, QuizQuestion, AnswerChoice, QuizResponse, QuizQuestionResponse


class AnswerChoiceSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = AnswerChoice
        fields = ('id', 'url', 'content', 'feedback')


class StudentAnswerChoiceSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = AnswerChoice
        fields = ('id', 'url', 'content')


class InstructorQuizQuestionListSerializer(serializers.ListSerializer):
    # Note: this is a cool example of how to implement bulk update.
    def update(self, instance, validated_data):
        question_mapping = {q.id: q for q in instance}
        data_mapping = {s['id']: s for s in validated_data}
        ret = []
        for question_id, data in data_mapping.items():
            if question_id not in question_mapping:
                raise PermissionDenied(detail='Question not in quiz')
            ret.append(self.child.update(question_mapping.get(question_id), data))
        return ret


class InstructorQuizQuestionSerializer(serializers.HyperlinkedModelSerializer):
    id = serializers.IntegerField(required=False)  # explicit so that it is available on list update
    answer_choices_detail = AnswerChoiceSerializer(many=True, read_only=True, source='answer_choices')

    class Meta:
        model = QuizQuestion
        fields = ('id', 'url', 'quiz', 'content', 'maximum_marks', 'question_type', 'answer_choices', 'sort_order',
                  'answer_choices_detail', 'correct_answers', 'correct_single_answer', 'feedback')
        list_serializer_class = InstructorQuizQuestionListSerializer


class InstructorQuizSerializer(serializers.HyperlinkedModelSerializer):
    quizquestion_set = InstructorQuizQuestionSerializer(many=True, read_only=True)

    def save(self, **kwargs):
        return super(InstructorQuizSerializer, self).save(category=TopicItem.QUIZ, **kwargs)

    class Meta:
        model = Quiz
        fields = ('id', 'url', 'topic', 'category', 'name', 'description', 'maximum_attempts', 'quizquestion_set',
                  'sort_order')
        read_only_fields = ('category',)


class StudentQuizQuestionSerializer(serializers.HyperlinkedModelSerializer):
    answer_choices_detail = StudentAnswerChoiceSerializer(many=True, read_only=True, source='answer_choices')

    class Meta:
        model = QuizQuestion
        fields = ('id', 'url', 'quiz', 'content', 'maximum_marks', 'question_type', 'sort_order',
                  'answer_choices_detail')


class StudentQuizSerializer(serializers.HyperlinkedModelSerializer):
    quizquestion_set = StudentQuizQuestionSerializer(many=True, read_only=True)

    class Meta:
        model = Quiz
        fields = ('id', 'url', 'topic', 'category', 'name', 'description', 'maximum_attempts', 'quizquestion_set',
                  'maximum_marks', 'sort_order')
        read_only_fields = ('category',)


class QuizQuestionResponseSerializer(serializers.HyperlinkedModelSerializer):
    """
    {
      "quiz_question": "http://127.0.0.1:8000/api/quizzes/questions/20/",
      "single_answer_response": "http://127.0.0.1:8000/api/quizzes/answer-choices/62/"
    }
    """
    response_content = serializers.StringRelatedField(source='single_answer_response.content')
    response_feedback = serializers.StringRelatedField(source='single_answer_response.feedback')
    correct_answer_content = serializers.StringRelatedField(source='quiz_question.correct_single_answer.content')
    question_feedback = serializers.StringRelatedField(source='quiz_question.feedback')

    class Meta:
        model = QuizQuestionResponse
        fields = ('id', 'url', 'quiz_question', 'single_answer_response', 'score', 'response_content',
                  'response_feedback', 'correct_answer_content', 'question_feedback')
        read_only_fields = ('score', 'response_content')


class QuizResponseSerializer(serializers.HyperlinkedModelSerializer):
    quizquestionresponse_set = QuizQuestionResponseSerializer(many=True)

    def create(self, validated_data):
        assert 'quiz' in validated_data, (
            'You must specify which quiz this response is for'
        )
        question_responses = validated_data.pop('quizquestionresponse_set')
        print question_responses
        response = QuizResponse.objects.create(**validated_data)
        # TODO: use bulk create in the future
        for question_response in question_responses:
            QuizQuestionResponse.objects.create(quiz_response=response, **question_response)
        return response

    class Meta:
        model = QuizResponse
        fields = ('id', 'url', 'student_user', 'quiz', 'quizquestionresponse_set', 'attempt', 'score', 'num_responses',
                  'created_timestamp', 'modified_timestamp')
        read_only_fields = ('student_user', 'quiz', 'total_score', 'attempt', 'score', 'num_responses',
                            'created_timestamp', 'modified_timestamp')
