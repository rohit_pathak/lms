from django.shortcuts import get_object_or_404
from rest_framework import generics
from rest_framework import status
from rest_framework.exceptions import PermissionDenied
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from course.api.permissions import course_instructor_or_forbidden, course_member_or_forbidden, \
    course_student_or_forbidden
from courseresource.api.views import BaseTopicItemList, BaseTopicItemDetail
from quiz.api.serializers import InstructorQuizSerializer, InstructorQuizQuestionSerializer, AnswerChoiceSerializer, \
    StudentQuizSerializer, QuizResponseSerializer, QuizQuestionResponseSerializer
from quiz.models import Quiz, QuizQuestion, AnswerChoice, QuizResponse, QuizQuestionResponse


class QuizList(BaseTopicItemList):
    queryset = Quiz.objects.all()
    serializer_class = InstructorQuizSerializer


class InstructorQuizDetail(BaseTopicItemDetail):
    """
    Quiz representation meant for instructors.
    """
    queryset = Quiz.objects.all()
    serializer_class = InstructorQuizSerializer

    def get(self, request, *args, **kwargs):
        course_instructor_or_forbidden(self.request.user, self.get_object().topic.course)
        return self.retrieve(request, *args, **kwargs)


class InstructorQuizQuestionList(generics.CreateAPIView):
    serializer_class = InstructorQuizQuestionSerializer
    queryset = QuizQuestion.objects.all()
    permission_classes = (IsAuthenticated,)

    def perform_create(self, serializer):
        course_instructor_or_forbidden(self.request.user, serializer.validated_data.get('quiz').topic.course)
        serializer.save()


class InstructorQuizQuestionBulkUpdate(APIView):
    permission_classes = (IsAuthenticated,)

    def put(self, request, pk, format=None):
        quiz = get_object_or_404(Quiz, pk=pk)
        course_instructor_or_forbidden(self.request.user, quiz.topic.course)
        serializer = InstructorQuizQuestionSerializer(quiz.quizquestion_set.all(), request.data, many=True)
        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        serializer.save()
        return Response(status.HTTP_200_OK)


class InstructorQuizQuestionDetail(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = InstructorQuizQuestionSerializer
    queryset = QuizQuestion.objects.all()
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        course_instructor_or_forbidden(self.request.user, self.get_object().quiz.topic.course)
        return self.retrieve(request, *args, **kwargs)

    def perform_update(self, serializer):
        course_instructor_or_forbidden(self.request.user, serializer.validated_data.get('quiz').topic.course)
        serializer.save()

    def perform_destroy(self, instance):
        course_instructor_or_forbidden(self.request.user, instance.quiz.topic.course)
        answer_choices = instance.answer_choices.all()
        answer_choices.delete()
        instance.delete()


class AnswerChoiceList(generics.CreateAPIView):
    serializer_class = AnswerChoiceSerializer
    queryset = AnswerChoice.objects.all()
    permission_classes = (IsAuthenticated,)
    # TODO: consider checking if user is instructor


class AnswerChoiceDetail(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = AnswerChoiceSerializer
    queryset = AnswerChoice.objects.all()
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        for question in self.get_object().quizquestion_set.all():
            course_member_or_forbidden(self.request.user, question.quiz.topic.course)
        return self.retrieve(request, *args, **kwargs)

    def perform_update(self, serializer):
        for question in self.get_object().quizquestion_set.all():
            course_instructor_or_forbidden(self.request.user, question.quiz.topic.course)
        serializer.save()

    def perform_destroy(self, instance):
        for question in instance.quizquestion_set.all():
            course_instructor_or_forbidden(self.request.user, question.quiz.topic.course)
        instance.delete()


class StudentQuizDetail(BaseTopicItemDetail):
    """
    Exposes a quiz representation that is appropriate for students.
    """
    queryset = Quiz.objects.all()
    serializer_class = StudentQuizSerializer
    permission_classes = (IsAuthenticated,)


class QuizQuestionResponseDetail(generics.ListAPIView):
    queryset = QuizQuestionResponse.objects.all()
    serializer_class = QuizQuestionResponseSerializer
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        raise PermissionDenied(detail="This operation isn't supported")


class QuizResponseList(generics.ListCreateAPIView):
    """
    Students submit responses to a quiz with POST.
    Students get their own responses to a quiz or instructors get all quiz responses on GET.
    """
    queryset = QuizResponse.objects.all()
    serializer_class = QuizResponseSerializer
    permission_classes = (IsAuthenticated,)

    def perform_create(self, serializer):
        # create only if user is student in course, and user has remaining attempts
        quiz = get_object_or_404(Quiz, pk=self.kwargs.get('pk'))
        course_student_or_forbidden(self.request.user, quiz.topic.course)
        current_attempt = QuizResponse.objects.filter(quiz=quiz, student_user=self.request.user).count() + 1
        if not current_attempt <= quiz.maximum_attempts:
            raise PermissionDenied(detail='You have exceeded the number of attempts allowed for this quiz.')
        # validate individual question responses point to questions and answers choices within the quiz
        questions = quiz.quizquestion_set.all()
        choices = AnswerChoice.objects.filter(quizquestion__in=questions)
        for answer in serializer.validated_data.get('quizquestionresponse_set'):
            if answer.get('quiz_question') not in questions or answer.get('single_answer_response') not in choices:
                raise PermissionDenied('Invalid responses.')
        serializer.save(quiz=quiz, student_user=self.request.user, attempt=current_attempt)

    def get_queryset(self):
        quiz = get_object_or_404(Quiz, pk=self.kwargs.get('pk'))
        if quiz.topic.course.has_as_instructor(self.request.user):
            return QuizResponse.objects.filter(quiz=quiz)
        if quiz.topic.course.has_as_student(self.request.user):
            return QuizResponse.objects.filter(quiz=quiz, student_user=self.request.user)


class QuizResponseDetail(generics.RetrieveAPIView):
    serializer_class = QuizResponseSerializer
    queryset = QuizResponse.objects.all()
    permission_classes = (IsAuthenticated,)

    def retrieve(self, request, *args, **kwargs):
        response = self.get_object()
        course_instructor_or_forbidden(self.request.user, response.quiz.topic.course)
        return super(QuizResponseDetail, self).retrieve(request, *args, **kwargs)
