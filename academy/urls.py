from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.academy_home, name='academy-home'),
    url(r'^admin/$', views.academy_admin_home, name='academy-admin-home-page'),
    url(r'^admin/courses/$', views.academy_admin_courses, name='academy-admin-courses-page'),
    url(r'^admin/courses/newcourse/$', views.academy_admin_courses, name='academy-admin-new-course-page'),
    url(r'^admin/students/$', views.academy_admin_students, name='academy-admin-students-page'),
    url(r'^admin/instructors/$', views.academy_admin_instructors, name='academy-admin-instructors-page'),
    url(r'^instructor/$', views.academy_instructor_home, name='academy-instructor-home-page'),
    url(r'^student/$', views.academy_student_home, name='academy-student-home-page'),

]
