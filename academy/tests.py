from django.contrib.auth import get_user_model
from django.core.urlresolvers import reverse
from django.test import TestCase, Client
from rest_framework import status

from academy.models import Academy
from instructor.models import Instructor
from student.models import Student

User = get_user_model()


def create_user(username, domain='coursetrack.org', password='password'):
    first, last = username.split('.', 1)
    user = User.objects.create_user(email='%s@%s' % (username, domain), password=password, first_name=first,
                                    last_name=last)
    return user


def login_user(client, username, domain='coursetrack.org', password='password'):
    email = '%s@%s' % (username, domain)
    client.login(email=email, password=password)


def add_student(academy, user):
    return Student.objects.create(academy=academy, user=user,
                                  identification='.'.join([user.first_name.lower(), user.last_name.lower()]))


def add_instructor(academy, user):
    return Instructor.objects.create(academy=academy, user=user,
                                     identification='.'.join([user.first_name.lower(), user.last_name.lower()]))


def create_some_users():
    usernames = ['albus.dumbledore', 'severus.snape', 'remus.lupin', 'rubeus.hagrid', 'harry.potter', 'ron.weasley',
                 'hermione.granger', 'draco.malfoy', 'cedric.diggory', 'gawri.ishwaran',
                 'sanjay.sinha', 'chayan.singh', 'shreyans.jain', 'rajroshan.sawhney', 'shambhavi.singh',
                 'rachita.singh', 'john.doe', 'jane.doe']
    users = {u: create_user(u) for u in usernames}
    return users


def create_some_academies():
    academies = {
        'sanskriti': Academy.objects.create(short_name='sanskriti', name='Sanskriti School'),
        'hogwarts': Academy.objects.create(short_name='hogwarts', name='Hogwarts School of Witchcraft and Wizardry')
    }
    return academies


def default_setup():
    """
    Creates academies and users using the above defaults and adds users appropriately as instructors or students.
    Assumes the test database is initialized and accessible.
    """
    print 'Setting up test users and academies...'
    users = create_some_users()
    academies = create_some_academies()

    hogwarts = academies['hogwarts']
    hogwarts.admins.add(users['albus.dumbledore'])
    for i in ['severus.snape', 'remus.lupin', 'rubeus.hagrid']:
        add_instructor(hogwarts, users[i])
    for s in ['harry.potter', 'ron.weasley', 'hermione.granger', 'cedric.diggory', 'draco.malfoy']:
        add_student(hogwarts, users[s])
    hogwarts.save()

    sanskriti = academies['sanskriti']
    sanskriti.admins.add(users['gawri.ishwaran'])
    for i in ['sanjay.sinha', 'chayan.singh']:
        add_instructor(sanskriti, users[i])
    for s in ['rajroshan.sawhney', 'shambhavi.singh', 'shreyans.jain', 'rachita.singh']:
        add_student(sanskriti, users[s])
    sanskriti.save()

    return users, academies


class AcademyHomeTests(TestCase):
    def setUp(self):
        self.client = Client()

    @classmethod
    def setUpTestData(cls):
        cls.users, cls.academies = default_setup()

    def test_academy_admin_home_is_only_visible_to_academy_admin(self):
        url = reverse('academy-admin-home-page', args=(self.academies['sanskriti'].short_name,))
        response = self.client.get(url)
        self.assertRedirects(response, '{}?next={}'.format(reverse('login-page'), url))
        # instructor
        login_user(self.client, 'sanjay.sinha')
        response = self.client.get(url)
        self.assertEqual(status.HTTP_403_FORBIDDEN, response.status_code)
        self.client.logout()
        # student
        login_user(self.client, 'shreyans.jain')
        response = self.client.get(url)
        self.assertEqual(status.HTTP_403_FORBIDDEN, response.status_code)
        self.client.logout()
        # random user not in academy
        login_user(self.client, 'jane.doe')
        response = self.client.get(url)
        self.assertEqual(status.HTTP_403_FORBIDDEN, response.status_code)
        self.client.logout()

    def test_academy_home_redirects_to_academy_student_dashboard_for_a_student(self):
        academy_home_url = reverse('academy-home', args=(self.academies.get('sanskriti').short_name,))
        academy_dashboard_url = reverse('academy-student-home-page', args=(self.academies.get('sanskriti').short_name,))
        login_user(self.client, 'shreyans.jain')
        response = self.client.get(academy_home_url)
        self.assertRedirects(response, academy_dashboard_url)

    def test_academy_home_redirects_to_academy_instructor_dashboard_for_an_instructor(self):
        academy_home_url = reverse('academy-home', args=(self.academies.get('sanskriti').short_name,))
        academy_dashboard_url = reverse('academy-instructor-home-page',
                                        args=(self.academies.get('sanskriti').short_name,))
        login_user(self.client, 'sanjay.sinha')
        response = self.client.get(academy_home_url)
        self.assertRedirects(response, academy_dashboard_url)

    def test_academy_home_redirects_to_academy_admin_dashboard_for_an_admin(self):
        academy_home_url = reverse('academy-home', args=(self.academies.get('sanskriti').short_name,))
        academy_dashboard_url = reverse('academy-admin-home-page',
                                        args=(self.academies.get('sanskriti').short_name,))
        login_user(self.client, 'gawri.ishwaran')
        response = self.client.get(academy_home_url)
        self.assertRedirects(response, academy_dashboard_url)

    def test_academy_instrcutor_dashboard_is_forbidden_to_students(self):
        instructor_dash_url = reverse('academy-instructor-home-page',
                                      args=(self.academies.get('sanskriti').short_name,))
        login_user(self.client, 'rachita.singh')
        self.assertEqual(status.HTTP_403_FORBIDDEN, self.client.get(instructor_dash_url).status_code)

    def test_academy_admin_dashboard_is_forbidden_to_instructors_and_students(self):
        admin_dash_url = reverse('academy-admin-home-page', args=(self.academies.get('sanskriti').short_name,))
        login_user(self.client, 'sanjay.sinha')
        self.assertEqual(status.HTTP_403_FORBIDDEN, self.client.get(admin_dash_url).status_code)
        self.client.logout()
        login_user(self.client, 'shreyans.jain')
        self.assertEqual(status.HTTP_403_FORBIDDEN, self.client.get(admin_dash_url).status_code)
