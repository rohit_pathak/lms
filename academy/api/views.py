import textwrap

from django.contrib.auth import get_user_model
from django.core.mail import EmailMessage
from django.core.urlresolvers import reverse
from django.core.validators import validate_email
from django.forms.forms import ValidationError
from django.shortcuts import get_object_or_404
from rest_framework import generics
from rest_framework.exceptions import PermissionDenied
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

import constants
from academy.api.permissions import IsAdminInAcademy, academy_admin_or_forbidden
from academy.models import Academy
from course.api.serializers import CourseRef
from instructor.api.serializers import InstructorRef
from invitation.api.serializers import InvitationSerializer
from invitation.models import Invitation
from serializers import AcademyRef, AcademySerializer
from student.api.serializers import StudentRef
from student.models import Student

User = get_user_model()


class AcademyList(generics.ListCreateAPIView):
    """
    All academies in the system.
    """
    queryset = Academy.objects.all()
    serializer_class = AcademyRef


class AcademyDetail(generics.RetrieveUpdateAPIView):
    """
    Detailed information about an academy.
    """
    queryset = Academy.objects.all()
    serializer_class = AcademySerializer
    permission_classes = (IsAuthenticated, IsAdminInAcademy,)


class AcademyDetailFromShortName(generics.RetrieveAPIView):
    queryset = Academy.objects.all()
    serializer_class = AcademySerializer
    permission_classes = (IsAuthenticated, IsAdminInAcademy,)
    lookup_field = 'short_name'


class AcademyInstructors(generics.ListAPIView):
    """
    All instructors teaching at this academy.
    """
    serializer_class = InstructorRef
    permission_classes = (IsAuthenticated, IsAdminInAcademy,)

    def get_queryset(self):
        academy = get_object_or_404(Academy, pk=self.kwargs['pk'])
        self.check_object_permissions(self.request, academy)
        return academy.instructor_set.all()


class AcademyStudents(generics.ListAPIView):
    """
    All students studying at this academy.
    """
    permission_classes = (IsAuthenticated,)
    serializer_class = StudentRef

    def get_queryset(self):
        academy = get_object_or_404(Academy, pk=self.kwargs['pk'])
        if not (academy.has_as_admin(self.request.user) or academy.has_as_instructor(self.request.user)):
            raise PermissionDenied('You are neither an instructor nor an admin in the academy.')
        return academy.student_set.filter(is_active=True)


class AcademyCourses(generics.ListAPIView):
    """
    All courses in the academy
    """
    permission_classes = (IsAuthenticated, IsAdminInAcademy,)
    serializer_class = CourseRef

    def get_queryset(self):
        academy = get_object_or_404(Academy, pk=self.kwargs['pk'])
        academy_admin_or_forbidden(self.request.user, academy)
        return academy.course_set.filter(is_active=True)


def invite(to_emails, from_user, academy, role, signup_url):
    invitations = [Invitation(invited_by=from_user, academy=academy, email=email, role=role) for email in to_emails]
    subject = "%s invited you as a %s" % (academy.name, role.capitalize())
    body = """
    Hello,
    %s from %s invited you to join Coursetrack as a student. Please use the link below to sign up for an account.
    %s

    Thanks,
    The Coursetrack Team
    """ % (from_user.get_full_name(), academy.name, signup_url)
    Invitation.objects.bulk_create(invitations)
    email = EmailMessage(subject=subject, body=textwrap.dedent(body),
                         to=['Undisclosed Receipents <noreply@coursetrack.org>'],
                         bcc=[i.email for i in invitations])
    email.send()
    return invitations


def add_students(students):
    new_additions = []
    for s in students:
        if s.pk is not None:
            s.is_active = True
            s.save()
        else:
            new_additions.append(s)
    Student.objects.bulk_create(new_additions)
    print 'sending email to new students (todo)'
    return students


class BatchAddStudents(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request, pk, format=None):
        academy = get_object_or_404(Academy, pk=pk)
        academy_admin_or_forbidden(self.request.user, academy)
        failures, invitation_emails, old_students, new_students, old_invitations, new_invitations = [], [], \
                                                                                                    [], [], [], []
        for raw_email in request.data.get('emails').split(','):
            email = raw_email.strip()
            try:
                validate_email(email)
                if academy.student_set.filter(user__email=email, is_active=True).exists():
                    old_students.append(academy.student_set.get(user__email=email))
                elif academy.student_set.filter(user__email=email, is_active=False).exists():
                    print 'old inactive student', academy.student_set.get(user__email=email, is_active=False)
                    new_students.append(academy.student_set.get(user__email=email, is_active=False))
                elif User.objects.filter(email=email).exists():
                    new_students.append(Student(user=User.objects.get(email=email), academy=academy))
                elif academy.invitation_set.filter(email=email).exists():
                    old_invitations.append(academy.invitation_set.get(email=email))
                else:
                    print 'no user or invite found with email', email
                    invitation_emails.append(email)
            except ValidationError:
                failures.append(email)
        new_invitations = invite(invitation_emails, self.request.user, academy, constants.STUDENT,
                                 request.build_absolute_uri(reverse('registration_register')))
        add_students(new_students)
        return Response({
            'new_students': [StudentRef(s, context={'request': request}).data for s in new_students],
            'old_students': [StudentRef(s, context={'request': request}).data for s in old_students],
            'new_invitations': [i.email for i in new_invitations],
            'old_invitations': [i.email for i in old_invitations],
            'failures': failures
        })


class AcademyStudentInvitations(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = InvitationSerializer

    def get_queryset(self):
        academy = get_object_or_404(Academy, pk=self.kwargs['pk'])
        academy_admin_or_forbidden(self.request.user, academy)
        return academy.invitation_set.filter(is_active=True, role=constants.STUDENT)
