from django.core.exceptions import PermissionDenied
from rest_framework.permissions import BasePermission


class IsAdminInAcademy(BasePermission):
    def has_object_permission(self, request, view, obj):
        return request.user.academy_set.filter(pk=obj.pk).exists()


class IsAdminInAcademyOfObject(BasePermission):
    def has_object_permission(self, request, view, obj):
        return request.user.academy_set.filter(pk=obj.academy.pk).exists()


class IsInstructorInAcademy(BasePermission):
    def has_object_permission(self, request, view, academy):
        return request.user.instructor_set.filter(academy=academy).exists()


class IsInstructorInAcademyOfObject(BasePermission):
    def has_object_permission(self, request, view, obj):
        return request.user.instructor_set.filter(academy=obj.academy).exists()


class IsStudentInAcademy(BasePermission):
    def has_object_permission(self, request, view, academy):
        return request.user.student_set.filter(academy=academy).exists()


# custom permission checks that raise exceptions directly

def academy_student_or_forbidden(user, academy):
    if not academy.has_as_student(user):
        raise PermissionDenied()


def academy_instructor_or_forbidden(user, academy):
    if not academy.has_as_instructor(user):
        raise PermissionDenied()


def academy_admin_or_forbidden(user, academy):
    if not academy.has_as_admin(user):
        raise PermissionDenied()


def academy_member_or_forbidden(user, academy):
    if not (academy.has_as_admin(user) or academy.has_as_student(user) or academy.has_as_instructor(user)):
        raise PermissionDenied()
