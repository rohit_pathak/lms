from rest_framework import serializers

from academy.models import Academy


class AcademyRef(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Academy
        fields = ('url', 'name', 'short_name')


class AcademySerializer(serializers.HyperlinkedModelSerializer):
    # TODO: links to students and instructors urls

    class Meta:
        model = Academy
        fields = ('url', 'name', 'short_name', 'admins')
