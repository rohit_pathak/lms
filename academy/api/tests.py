from django.contrib.auth import get_user_model
from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase, APIRequestFactory

import academy.tests as td
from invitation.models import Invitation
from student.models import Student

User = get_user_model()


class AcademyDetailViewTests(APITestCase):
    def setUp(self):
        self.factory = APIRequestFactory()

    @classmethod
    def setUpTestData(cls):
        cls.users, cls.academies = td.default_setup()

    def test_get_academy_detail(self):
        academy = self.academies.get('sanskriti')
        url = reverse('academy-detail', args=(academy.pk,))
        request = self.factory.get(url)
        td.login_user(self.client, 'sanjay.sinha')
        self.assertEqual(status.HTTP_403_FORBIDDEN, self.client.get(url).status_code)
        self.client.logout()
        td.login_user(self.client, 'gawri.ishwaran')
        response = self.client.get(url)
        self.assertEqual(response.data,
                         {'url': reverse('academy-detail', args=(academy.pk,), request=request),
                          'name': academy.name,
                          'short_name': academy.short_name,
                          'admins': [reverse('customuser-detail', args=(u.pk,), request=request) for u in
                                     academy.admins.all()]})
        self.client.logout()

    def test_batch_add_students_for_unregistered_accounts_creates_invitations(self):
        academy = self.academies.get('sanskriti')
        url = reverse('academy-add-students', args=(academy.pk,))
        # test with new emails that haven't been invited
        data = {'emails': 'batman@gotham.com, superman@coursetrack.org'}
        td.login_user(self.client, 'gawri.ishwaran')
        response = self.client.post(url, data=data)
        self.assertEqual(response.data, {
            'new_students': [],
            'old_students': [],
            'new_invitations': ['batman@gotham.com', 'superman@coursetrack.org'],
            'old_invitations': [],
            'failures': []
        })
        self.assertTrue(Invitation.objects.filter(email='batman@gotham.com').exists())
        self.assertTrue(Invitation.objects.filter(email='superman@coursetrack.org').exists())
        # test with emails that have been invited
        data = {'emails': 'batman@gotham.com, spiderman@nyc.com'}
        response = self.client.post(url, data=data)
        self.assertEqual(response.data, {
            'new_students': [],
            'old_students': [],
            'new_invitations': ['spiderman@nyc.com'],
            'old_invitations': ['batman@gotham.com'],
            'failures': []
        })
        self.assertTrue(Invitation.objects.filter(email='batman@gotham.com').exists())
        self.assertTrue(Invitation.objects.filter(email='superman@coursetrack.org').exists())
        self.assertTrue(Invitation.objects.filter(email='spiderman@nyc.com').exists())
        self.client.logout()

    def test_batch_add_students_failues(self):
        academy = self.academies.get('sanskriti')
        url = reverse('academy-add-students', args=(academy.pk,))
        data = {'emails': 'batman, superman.org, !@3122'}
        td.login_user(self.client, 'gawri.ishwaran')
        response = self.client.post(url, data=data)
        self.assertEqual(response.data, {
            'new_students': [],
            'old_students': [],
            'new_invitations': [],
            'old_invitations': [],
            'failures': ['batman', 'superman.org', '!@3122']
        })
        self.client.logout()

    def test_new_registration_for_invited_students_adds_them_to_academy(self):
        academy = self.academies.get('sanskriti')
        email = 'amal.sahai@abc.com'
        url = reverse('academy-add-students', args=(academy.pk,))
        data = {'emails': email}
        td.login_user(self.client, 'gawri.ishwaran')
        response = self.client.post(url, data=data)
        self.assertEqual(response.data['new_invitations'], [email])
        self.client.logout()
        register_url = reverse('registration_register')
        self.client.post(register_url, data={'first_name': 'Amal', 'last_name': 'Sahai', 'email': email,
                                             'password1': 'coursetrack123', 'password2': 'coursetrack123'})
        activation_key = User.objects.get(email=email).registrationprofile.activation_key
        self.client.get(reverse('registration_activate', args=(activation_key,)))
        self.assertTrue(Student.objects.filter(academy=academy, user=User.objects.get(email=email)).exists())
