from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns

from academy.api import views

urlpatterns = [
    url(r'^$', views.AcademyList.as_view(), name='academy-list'),
    # the following urls only apply if the user is an academy admin
    url(r'^(?P<pk>\d+)/$', views.AcademyDetail.as_view(), name='academy-detail'),
    url(r'^(?P<pk>\d+)/students/$', views.AcademyStudents.as_view(), name='academy-students'),
    url(r'^(?P<pk>\d+)/instructors/$', views.AcademyInstructors.as_view(), name='academy-instructors'),
    url(r'^(?P<pk>\d+)/courses/$', views.AcademyCourses.as_view(), name='academy-courses'),
    url(r'^(?P<pk>\d+)/student-invitations/$', views.AcademyStudentInvitations.as_view(),
        name='academy-student-invitations'),
    url(r'^(?P<pk>\d+)/add-students/$', views.BatchAddStudents.as_view(), name='academy-add-students'),
    url(r'^(?P<short_name>[\w\d]+)/$', views.AcademyDetailFromShortName.as_view()),

]

urlpatterns = format_suffix_patterns(urlpatterns)
