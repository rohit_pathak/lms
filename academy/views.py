from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render

from academy.api.permissions import academy_member_or_forbidden, academy_admin_or_forbidden, \
    academy_instructor_or_forbidden, academy_student_or_forbidden
from academy.models import Academy


@login_required
def academy_home(request, short_name):
    academy = get_object_or_404(Academy, short_name=short_name)
    academy_member_or_forbidden(request.user, academy)
    if academy.has_as_instructor(request.user):
        return HttpResponseRedirect(reverse('academy-instructor-home-page', args=[short_name]))
    if academy.has_as_admin(request.user):
        return HttpResponseRedirect(reverse('academy-admin-home-page', args=[short_name]))
    if academy.has_as_student(request.user):
        return HttpResponseRedirect(reverse('academy-student-home-page', args=[short_name]))


@login_required
def academy_admin_home(request, short_name):
    academy = get_object_or_404(Academy, short_name=short_name)
    academy_admin_or_forbidden(request.user, academy)
    context = {'academy': academy}
    return render(request, 'academy/academy-admin-home.html', context)


@login_required
def academy_admin_courses(request, short_name):
    academy = get_object_or_404(Academy, short_name=short_name)
    academy_admin_or_forbidden(request.user, academy)
    context = {'academy': academy}
    return render(request, 'academy/academy-admin-courses.html', context)


@login_required
def academy_admin_instructors(request, short_name):
    academy = get_object_or_404(Academy, short_name=short_name)
    academy_admin_or_forbidden(request.user, academy)
    context = {'academy': academy}
    return render(request, 'academy/academy-admin-instructors.html', context)


@login_required
def academy_admin_students(request, short_name):
    academy = get_object_or_404(Academy, short_name=short_name)
    academy_admin_or_forbidden(request.user, academy)
    context = {'academy': academy, 'students': academy.student_set.filter(is_active=True)}
    return render(request, 'academy/academy-admin-students.html', context)


@login_required
def academy_instructor_home(request, short_name):
    academy = get_object_or_404(Academy, short_name=short_name)
    academy_instructor_or_forbidden(request.user, academy)
    instructor_profile = academy.instructor_set.get(user=request.user)
    context = {'academy': academy, 'courses': instructor_profile.course_set.filter(is_active=True)}
    return render(request, 'academy/academy-instructor-home.html', context)


@login_required
def academy_student_home(request, short_name):
    academy = get_object_or_404(Academy, short_name=short_name)
    academy_student_or_forbidden(request.user, academy)
    student_profile = academy.student_set.get(user=request.user)
    context = {'academy': academy, 'courses': student_profile.course_set.filter(is_active=True)}
    return render(request, 'academy/academy-student-home.html', context)
