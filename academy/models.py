from __future__ import unicode_literals

from django.conf import settings
from django.db import models


class Academy(models.Model):
    name = models.CharField(max_length=50)
    short_name = models.CharField(max_length=20, unique=True)
    admins = models.ManyToManyField(settings.AUTH_USER_MODEL)

    def __unicode__(self, ):
        return '%s (%s)' % (self.name, self.short_name)

    def has_as_instructor(self, user):
        return self.instructor_set.filter(user=user, is_active=True).exists()

    def has_as_student(self, user):
        return self.student_set.filter(user=user, is_active=True).exists()

    def has_as_admin(self, user):
        return user in self.admins.all()

    def get_active_courses(self):
        return self.course_set.filter(is_active=True)
