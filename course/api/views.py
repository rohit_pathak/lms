from django.shortcuts import get_object_or_404
from rest_framework import generics, status
from rest_framework.exceptions import PermissionDenied
from rest_framework.parsers import MultiPartParser
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

import utils
from academy.api.permissions import academy_admin_or_forbidden
from announcement.api.serializers import AnnouncementRef
from course.api.permissions import IsAdminOrCourseInstructorOrReadOnlyForStudents, IsCourseMemberOrAcademyAdmin, \
    course_member_or_forbidden, course_instructor_or_forbidden, course_student_or_forbidden
from course.api.serializers import CourseRef, CourseDetailSerializer
from course.models import Course
from courseresource.api.serializers import ResourceTopicSerializer, ResourceTopicRef
from courseresource.models import ResourceTopic
from document.api.serializers import DocumentSerializer
from document.models import create_document
from forum.api.serializers import QuestionRef
from gradebook.api.serializers import TopicSerializer, GradeSerializer
from instructor.api.serializers import InstructorRef
from notification.api.serializers import NotificationSerializer
from notification.models import Notification
from student.api.serializers import StudentRef
from student.models import Student
from timeline.api.serializers import StudentEventSerializer


def have_same_academy(academy, members):
    for member in members:
        if member.academy != academy:
            return False
    return True


class CourseList(generics.CreateAPIView):
    """
    Create a course.
    A user can create a course if he/she is an instructor in
    the academy the course is to be created in.
    """
    serializer_class = CourseRef
    queryset = Course.objects.all()
    permission_classes = (IsAuthenticated,)

    def perform_create(self, serializer):
        academy_admin_or_forbidden(self.request.user, serializer.validated_data.get('academy'))
        if len(serializer.validated_data.get('instructors')) and not have_same_academy(
                serializer.validated_data.get('academy'), serializer.validated_data.get('instructors')):
            raise PermissionDenied('All instructors in the course must be in the same academy that the course is in.')
        serializer.save()


class CourseDetail(generics.RetrieveUpdateAPIView):
    """
    Get or update a course.
    Only course instructors can update.
    Only course instructors and students or course academy admins can get.
    """
    queryset = Course.objects.all()
    serializer_class = CourseDetailSerializer
    permission_classes = (IsAuthenticated, IsAdminOrCourseInstructorOrReadOnlyForStudents)

    def perform_update(self, serializer):
        if len(serializer.validated_data.get('instructors', [])) and not have_same_academy(
                serializer.validated_data.get('academy'), serializer.validated_data.get('instructors')):
            raise PermissionDenied(
                detail='All instructors in the course must be in the same academy that the course is in.')
        if len(serializer.validated_data.get('students', [])) and not have_same_academy(
                serializer.validated_data.get('academy'), serializer.validated_data.get('students')):
            raise PermissionDenied(
                detail='All students in the course must be in the same academy that the course is in.')
        serializer.save()


class CourseStudents(generics.ListAPIView):
    """
    Get all students in a course. All members of a course have access to this information
    """
    serializer_class = StudentRef
    permission_classes = (IsAuthenticated, IsCourseMemberOrAcademyAdmin,)

    def get_queryset(self):
        course = get_object_or_404(Course, pk=self.kwargs['pk'])
        self.check_object_permissions(self.request, course)
        return course.students.filter(is_active=True)


class CourseInstructors(generics.ListAPIView):
    """
    Get all instructors teaching this course. All members of a course have access to this information
    """
    serializer_class = InstructorRef
    permission_classes = (IsAuthenticated, IsCourseMemberOrAcademyAdmin)

    def get_queryset(self):
        course = get_object_or_404(Course, pk=self.kwargs['pk'])
        self.check_object_permissions(self.request, course)
        return course.instructors.all()


class CourseStudentNotifications(generics.ListAPIView):
    serializer_class = NotificationSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        course = get_object_or_404(Course, pk=self.kwargs['pk'])
        course_student_or_forbidden(self.request.user, course)
        return course.notification_set.filter(notify_to=Notification.STUDENTS)


class CourseInstructorNotifications(generics.ListAPIView):
    serializer_class = NotificationSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        course = get_object_or_404(Course, pk=self.kwargs['pk'])
        course_instructor_or_forbidden(self.request.user, course)
        return course.notification_set.filter(notify_to=Notification.INSTRUCTORS)


class CourseStudentEvents(generics.ListAPIView):
    serializer_class = StudentEventSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        course = get_object_or_404(Course, pk=self.kwargs['pk'])
        course_student_or_forbidden(self.request.user, course)
        return course.studentevent_set.all()


class CourseResources(APIView):
    serializer_class = ResourceTopicSerializer
    permission_classes = (IsAuthenticated,)

    def get(self, request, pk, format=None):
        course = get_object_or_404(Course, pk=pk)
        course_member_or_forbidden(request.user, course)
        serializer = ResourceTopicSerializer(course.resourcetopic_set.all(), many=True, context={'request': request})
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        course = get_object_or_404(Course, pk=pk)
        course_instructor_or_forbidden(request.user, course)
        serializers = []
        # TODO: use list serializer instead (for speed, I guess)
        for raw_topic in request.data:
            topic = get_object_or_404(ResourceTopic, course=course, pk=raw_topic['id'])
            serializer = ResourceTopicRef(topic, data=raw_topic, context={'request': request})
            if not serializer.is_valid():
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
            serializers.append(serializer)
        for serializer in serializers:
            serializer.save()
        return Response(status=status.HTTP_200_OK)


class CourseQuestions(generics.ListAPIView):
    serializer_class = QuestionRef
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        course = get_object_or_404(Course, pk=self.kwargs.get('pk'))
        course_member_or_forbidden(self.request.user, course)
        return course.question_set.all()


class CourseAnnouncements(generics.ListAPIView):
    serializer_class = AnnouncementRef
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        course = get_object_or_404(Course, pk=self.kwargs['pk'])
        course_member_or_forbidden(self.request.user, course)
        return course.announcement_set.all()


class CourseGradeTopics(generics.ListAPIView):
    """
    Get all gradebook topics in this course.
    """
    serializer_class = TopicSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        course = get_object_or_404(Course, pk=self.kwargs['pk'])
        course_member_or_forbidden(self.request.user, course)
        return course.topic_set.all()


class StudentCourseGrades(generics.ListAPIView):
    """
    Get all grades for a student in this course.
    """
    serializer_class = GradeSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        course = get_object_or_404(Course, pk=self.kwargs['pk'])
        course_member_or_forbidden(self.request.user, course)
        student = get_object_or_404(Student, pk=self.kwargs['student_id'])
        if not course.has_as_instructor(self.request.user) and self.request.user != student.user:
            raise PermissionDenied(detail='These grades are not yours.')
        return student.grade_set.filter(topic__course=course)


class CourseDocuments(APIView):
    """
    Upload a file for an instructor or retrieve all files uploaded by an instructor.
    """
    permission_classes = (IsAuthenticated,)
    parser_classes = (MultiPartParser,)

    def get(self, request, *args, **kwargs):
        course = get_object_or_404(Course, pk=self.kwargs.get('pk'))
        course_instructor_or_forbidden(self.request.user, course)
        serializer = DocumentSerializer(course.documents.all(), many=True, context={'request': request})
        return Response(serializer.data)

    def post(self, request, *args, **kwargs):
        course = get_object_or_404(Course, pk=self.kwargs.get('pk'))
        course_instructor_or_forbidden(self.request.user, course)
        file_object = request.data['file']
        # append number to name if file with this name already exists
        file_key = utils.generate_document_key_name(file_object.name, course.academy.short_name, course_id=course.id)
        document = create_document(file_object, file_key, request.user)
        course.documents.add(document)
        course.save()
        return Response(data=DocumentSerializer(document, context={'request': request}).data, status=204)
