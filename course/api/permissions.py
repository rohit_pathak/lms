from django.core.exceptions import PermissionDenied
from rest_framework import permissions


class IsCourseMemberOrAcademyAdmin(permissions.BasePermission):
    def has_object_permission(self, request, view, course):
        return course.has_as_member(request.user) or course.academy.has_as_admin(request.user)


class IsAdminOrCourseInstructorOrReadOnlyForStudents(permissions.BasePermission):
    def has_object_permission(self, request, view, course):
        if course.has_as_instructor(request.user) or course.academy.has_as_admin(request.user):
            return True
        if course.has_as_student(request.user) and request.method in permissions.SAFE_METHODS:
            return True
        return False


class IsCourseInstructorOfResource(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        return obj.course.has_as_instructor(request.user)


class IsCourseInstructorOfResourceOrStudentReadOnly(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        if obj.course.has_as_instructor(request.user) or (
                        request.method in permissions.SAFE_METHODS and obj.course.has_as_student(request.user)):
            return True
        return False


def course_instructor_or_forbidden(user, course):
    if not course.has_as_instructor(user):
        raise PermissionDenied()


def course_student_or_forbidden(user, course):
    if not course.has_as_student(user):
        raise PermissionDenied()


def course_member_or_forbidden(user, course):
    if not (course.has_as_student(user) or course.has_as_instructor(user)):
        raise PermissionDenied()


def document_in_course_or_forbidden(document, course):
    if not course.documents.filter(pk=document.pk).exists():
        raise PermissionDenied()
