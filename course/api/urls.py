from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns

from course.api import views

urlpatterns = [
    url(r'^$', views.CourseList.as_view(), name='course-list'),
    url(r'^(?P<pk>\d+)/$', views.CourseDetail.as_view(), name='course-detail'),
    url(r'^(?P<pk>\d+)/students/$', views.CourseStudents.as_view(), name='course-students'),
    url(r'^(?P<pk>\d+)/instructors/$', views.CourseInstructors.as_view(), name='course-instructors'),
    url(r'^(?P<pk>\d+)/student-notifications/$', views.CourseStudentNotifications.as_view(),
        name='course-student-notifications'),  # deprecated
    url(r'^(?P<pk>\d+)/instructor-notifications/$', views.CourseInstructorNotifications.as_view(),
        name='course-instructor-notifications'),  # deprecated
    url(r'^(?P<pk>\d+)/student-events/$', views.CourseStudentEvents.as_view(),
        name='course-student-events'),
    url(r'^(?P<pk>\d+)/resources/$', views.CourseResources.as_view(), name='course-resources'),
    url(r'^(?P<pk>\d+)/questions/$', views.CourseQuestions.as_view(), name='course-questions'),
    url(r'^(?P<pk>\d+)/announcements/$', views.CourseAnnouncements.as_view(), name='course-announcements'),
    url(r'^(?P<pk>\d+)/gradetopics/$', views.CourseGradeTopics.as_view(), name='course-grade-topics'),
    url(r'^(?P<pk>\d+)/students/(?P<student_id>\d+)/grades/$', views.StudentCourseGrades.as_view(),
        name='student-course-grades'),
    url(r'^(?P<pk>\d+)/documents/$', views.CourseDocuments.as_view(), name='course-documents'),
]

urlpatterns = format_suffix_patterns(urlpatterns)
