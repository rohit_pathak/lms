from django.core.mail import EmailMessage
from django.core.urlresolvers import reverse
from rest_framework import serializers

from course.models import Course
from instructor.api.serializers import InstructorRef


class CourseRef(serializers.HyperlinkedModelSerializer):
    academy_name = serializers.StringRelatedField(source='academy.name', read_only=True)
    instructor_details = InstructorRef(read_only=True, many=True, source='instructors')

    class Meta:
        model = Course
        fields = ('url', 'id', 'name', 'description', 'create_date', 'academy', 'academy_name', 'instructors',
                  'instructor_details')

    def validate(self, data):
        return data


class CourseDetailSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Course
        fields = ('url', 'id', 'name', 'description', 'academy', 'instructors', 'students')

    def __get_removed_students(self, existing, incoming):
        return [s for s in existing if s not in incoming]

    def __get_added_students(self, existing, incoming):
        return [s for s in incoming if s not in existing]

    def update(self, instance, validated_data):
        assert self.context['request'] is not None
        course, academy = instance, instance.academy
        welcome_email_subject = 'Welcome to %s' % (course.name,)
        welcome_email_body = """
        Hello %s:

        You have been added to the course "%s" at %s.
        Welcome! Check out the course website at %s.

        Thanks,
        The Coursetrack Team
        """
        remove_email_subject = 'Unenrolled from %s' % (course.name,)
        remove_email_body = """
        Hello %s:

        You were unenrolled from "%s" at %s.
        If this was done by mistake, please ask your instructor to add you back to the course.

        Thanks,
        The Coursetrack Team
        """
        for student in self.__get_added_students(instance.students.all(), validated_data.get('students', [])):
            email = EmailMessage(subject=welcome_email_subject, to=[student.user.email],
                                 body=welcome_email_body % (student.user.first_name, course.name, academy.name,
                                                            self.context['request'].build_absolute_uri(
                                                                reverse('course-home-page', args=(course.pk,)))))
            email.send(fail_silently=True)
        for student in self.__get_removed_students(instance.students.all(), validated_data.get('students', [])):
            email = EmailMessage(subject=remove_email_subject, to=[student.user.email],
                                 body=remove_email_body % (student.user.first_name, course.name, academy.name))
            email.send(fail_silently=True)
        return super(CourseDetailSerializer, self).update(instance, validated_data)
