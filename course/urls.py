from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^(?P<pk>\d+)/$', views.course_home, name='course-home-page'),
    url(r'^(?P<pk>\d+)/resources/$', views.course_resources, name='course-resources-page'),
    url(r'^(?P<pk>\d+)/resources/items/(?P<item_id>\d+)/$', views.course_resource_item_detail,
        name='course-resource-item-detail-page'),
    url(r'^(?P<pk>\d+)/forum/$', views.course_forum, name='course-forum-page'),
    url(r'^(?P<pk>\d+)/forum/questions/(?P<question_id>\d+)$', views.course_forum_question_detail,
        name='course-forum-question-detail-page'),
    url(r'^(?P<pk>\d+)/announcements/$', views.course_announcements, name='course-announcements-page'),
    url(r'^(?P<pk>\d+)/announcements/(?P<announcement_id>\d+)/$', views.course_announcement_detail,
        name='course-announcement-detail-page'),
    url(r'^(?P<pk>\d+)/gradebook/$', views.course_gradebook, name='course-gradebook-page'),

    # admin urls
    url(r'^(?P<pk>\d+)/admin/$', views.course_admin_home, name='course-admin-home-page'),
    url(r'^(?P<pk>\d+)/admin/resources/$', views.course_admin_resources, name='course-admin-resources-page'),
    url(r'^(?P<pk>\d+)/admin/resources/quizzes/(?P<quiz_id>\d+)/$', views.course_admin_resources_quiz_detail,
        name='course-admin-resources-quiz-detail-page'),
    url(r'^(?P<pk>\d+)/admin/forum/$', views.course_admin_forum, name='course-admin-forum-page'),
    url(r'^(?P<pk>\d+)/admin/forum/questions/(?P<question_id>\d+)/$', views.course_admin_forum_question_detail,
        name='course-admin-forum-question-detail-page'),
    url(r'^(?P<pk>\d+)/admin/announcements/$', views.course_admin_announcements,
        name='course-admin-announcements-page'),
    url(r'^(?P<pk>\d+)/admin/gradebook/$', views.course_admin_gradebook, name='course-admin-gradebook-page'),
    url(r'^(?P<pk>\d+)/admin/students/$', views.course_admin_students, name='course-admin-students-page'),
]
