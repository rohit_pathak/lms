from django.contrib.auth.decorators import login_required
from django.core.exceptions import PermissionDenied
from django.shortcuts import render, get_object_or_404

from announcement.models import Announcement
from course.api.permissions import course_instructor_or_forbidden, course_student_or_forbidden
from course.models import Course
from courseresource.models import TopicItem
from forum.models import Question


def _render_course(request, pk, page):
    course = get_object_or_404(Course, pk=pk)
    course_student_or_forbidden(request.user, course)
    context = {'academy': course.academy, 'course': course}
    return render(request, page, context)


@login_required
def course_home(request, pk):
    return _render_course(request, pk, 'course/course-home.html')


@login_required
def course_resources(request, pk):
    return _render_course(request, pk, 'course/course-resources.html')


@login_required
def course_resource_item_detail(request, pk, item_id):
    course = get_object_or_404(Course, pk=pk)
    course_student_or_forbidden(request.user, course)
    item = get_object_or_404(TopicItem, pk=item_id)
    if item.topic.course != course:
        raise PermissionDenied()
    context = {'academy': course.academy, 'course': course, 'topic_item': item}
    if item.category == TopicItem.QUIZ:
        return render(request, 'course/course-quiz-detail.html', context)
    else:
        return render(request, 'course/course-resource-item-detail.html', context)


@login_required
def course_forum(request, pk):
    return _render_course(request, pk, 'course/course-forum.html')


@login_required
def course_forum_question_detail(request, pk, question_id):
    course = get_object_or_404(Course, pk=pk)
    course_student_or_forbidden(request.user, course)
    question = get_object_or_404(Question, pk=question_id, course=course)
    context = {'academy': course.academy, 'course': course, 'question': question}
    return render(request, 'course/course-forum-question-detail.html', context)


@login_required
def course_gradebook(request, pk):
    return _render_course(request, pk, 'course/course-gradebook.html')


@login_required
def course_announcements(request, pk):
    course = get_object_or_404(Course, pk=pk)
    course_student_or_forbidden(request.user, course)
    context = {'academy': course.academy, 'course': course,
               'announcements': course.announcement_set.all().order_by('-create_date')}
    return render(request, 'course/course-announcements.html', context)


@login_required
def course_announcement_detail(request, pk, announcement_id):
    course = get_object_or_404(Course, pk=pk)
    course_student_or_forbidden(request.user, course)
    announcement = get_object_or_404(Announcement, pk=announcement_id)
    context = {'academy': course.academy, 'course': course, 'announcement': announcement}
    return render(request, 'course/course-announcement-detail.html', context)


def _render_course_admin(request, pk, page):
    course = get_object_or_404(Course, pk=pk)
    course_instructor_or_forbidden(request.user, course)
    context = {'academy': course.academy, 'course': course}
    return render(request, page, context)


@login_required
def course_admin_home(request, pk):
    return _render_course_admin(request, pk, 'course/admin/course-admin-home.html')


@login_required
def course_admin_resources(request, pk):
    return _render_course_admin(request, pk, 'course/admin/course-admin-resources.html')


@login_required
def course_admin_resources_quiz_detail(request, pk, quiz_id):
    return _render_course_admin(request, pk, 'course/admin/course-admin-resources-quiz-detail.html')


@login_required
def course_admin_forum(request, pk):
    return _render_course_admin(request, pk, 'course/admin/course-admin-forum.html')


@login_required
def course_admin_forum_question_detail(request, pk, question_id):
    course = get_object_or_404(Course, pk=pk)
    course_instructor_or_forbidden(request.user, course)
    question = get_object_or_404(Question, pk=question_id, course=course)
    context = {'academy': course.academy, 'course': course, 'question': question}
    return render(request, 'course/admin/course-admin-forum-question-detail.html', context)


@login_required
def course_admin_announcements(request, pk):
    return _render_course_admin(request, pk, 'course/admin/course-admin-announcements.html')


@login_required
def course_admin_gradebook(request, pk):
    return _render_course_admin(request, pk, 'course/admin/course-admin-gradebook.html')


@login_required
def course_admin_students(request, pk):
    return _render_course_admin(request, pk, 'course/admin/course-admin-students.html')
