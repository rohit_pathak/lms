from __future__ import unicode_literals

from django.db import models

from academy.models import Academy
from document.models import Document
from instructor.models import Instructor
from student.models import Student


class Course(models.Model):
    name = models.CharField(max_length=50)
    description = models.TextField()
    academy = models.ForeignKey(Academy)
    instructors = models.ManyToManyField(Instructor, blank=True)
    students = models.ManyToManyField(Student, blank=True)
    documents = models.ManyToManyField(Document, blank=True)
    is_active = models.BooleanField(default=True)
    create_date = models.DateTimeField(auto_now_add=True, blank=True)

    def __unicode__(self, ):
        return '%s | %s' % (self.name, self.academy)

    def has_as_instructor(self, user):
        return self.instructors.filter(user=user, is_active=True).exists()

    def has_as_student(self, user):
        return self.students.filter(user=user, is_active=True).exists()

    def has_as_member(self, user):
        return self.has_as_instructor(user) or self.has_as_student(user)
